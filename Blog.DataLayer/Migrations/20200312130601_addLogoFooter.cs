﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Blog.DataLayer.Migrations
{
    public partial class addLogoFooter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Footer",
                table: "Options",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "Options",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Footer",
                table: "Options");

            migrationBuilder.DropColumn(
                name: "Logo",
                table: "Options");
        }
    }
}
