﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Blog.DataLayer.Migrations
{
    public partial class addVideoToGallery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasVideoe",
                table: "Sliders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "VideoCode",
                table: "Sliders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VideoName",
                table: "Sliders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasVideoe",
                table: "Sliders");

            migrationBuilder.DropColumn(
                name: "VideoCode",
                table: "Sliders");

            migrationBuilder.DropColumn(
                name: "VideoName",
                table: "Sliders");
        }
    }
}
