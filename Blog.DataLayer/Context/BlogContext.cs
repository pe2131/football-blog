﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.DomainClasses;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Comment;
using Blog.DomainClasses.Option;
using Microsoft.EntityFrameworkCore;
using Blog.DomainClasses.Post;
using Blog.DomainClasses.PostCategory;
using Blog.DomainClasses.PostTag;
using Blog.DomainClasses.Slider;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Blog.DomainClasses.Teames;
using Blog.DomainClasses.Ranking;
using Blog.DomainClasses.Results;
using Blog.DomainClasses.schedules;
using Blog.DomainClasses.commentForSlider;
using System.Linq;
using Blog.DomainClasses.league;

namespace Blog.DataLayer.Context
{
    public class BlogContext : IdentityDbContext<ApplicationUser>
    {
        public BlogContext(DbContextOptions<BlogContext> options) : base(options)
        {

        }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<PostCategory> PostCategories { get; set; }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<CommentForSlider> CommentsForSlider { get; set; }

        public DbSet<PostTag> PostTags { get; set; }

        public DbSet<Slider> Sliders { get; set; }

        public DbSet<Option> Options { get; set; }
        public DbSet<Teams> Teams { get; set; }
        public DbSet<Ranking> Rankings { get; set; }
        public DbSet<Results> Results { get; set; }
        public DbSet<schedules> schedules { get; set; }
        public DbSet<League> leagues { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}
