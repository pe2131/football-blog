﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Utilities.Images
{
    public class Upload:IDisposable
    {
        private FileStream _stream;

        public void UploadImage(string path, IFormFile file)
        {
            _stream = new FileStream(path, FileMode.Create);
            file.CopyTo(_stream);
            _stream.Dispose();
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

    }
}
