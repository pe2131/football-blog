﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Utilities.String
{
    public static class URL
    {
        public static string ToUrl(this string name)
        {
            return name.Replace(" ", "-");
        }
    }
}
