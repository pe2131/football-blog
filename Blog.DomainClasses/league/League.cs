﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.DomainClasses.league
{
   public class League
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Teames.Teams> Teames { get; set; }

    }
}
