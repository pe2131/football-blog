﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.DomainClasses.commentForSlider
{
    public class CommentForSlider
    {
        [Key]
        public int CommentId { get; set; }
        public int SliderId { get; set; }
        public int? ParentId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(500, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(4000, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Text")]
        public string CommentText { get; set; }
        public string Subject { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        [Display(Name = "Publish status")]
        public bool IsPublish { get; set; }


        //Relations
        [Display(Name = "Slider")]
        public virtual Slider.Slider Slider { get; set; }
        public virtual CommentForSlider Parent { get; set; }

        public ICollection<CommentForSlider> SubComments { get; set; }

    }
}
