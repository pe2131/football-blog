﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.DomainClasses.Slider
{
    public class Slider
    {
        [Key]
        public int SliderId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(800, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Title")]
        public string Title { get; set; }
        public string ImageName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}.")]
        [MaxLength(250, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Address")]
        public string Url { get; set; }
        public DateTime CreateDate { get; set; }
        [Display(Name = "Views")]
        public int VisitCount { get; set; }
        [Display(Name = "Is publish")]
        public bool IsPublish { get; set; }
        [Display(Name = "Has Videoe")]
        public bool HasVideoe { get; set; }
        public string VideoName { get; set; }
        public string VideoCode { get; set; }

        public ICollection<commentForSlider.CommentForSlider> Comments { get; set; }
    }
}
