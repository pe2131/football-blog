﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.DomainClasses.schedules
{
    public class schedules
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int HomeTeamId { get; set; }
        [Required]
        public int AwayTeamId { get; set; }
        [Required]
        public DateTime MatchDay { get; set; }


        [ForeignKey(nameof(HomeTeamId))]
        public virtual Teames.Teams HomeTeams { get; set; }
        [ForeignKey(nameof(AwayTeamId))]
        public virtual Teames.Teams AwayTeams { get; set; }
    }
}
