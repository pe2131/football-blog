﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.DomainClasses.Ranking
{
    public class Ranking
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int TeamId { get; set; }
        [Required]
        public int Rank { get; set; }
        [Required]
        public int GF { get; set; }
        [Required]
        public int GD { get; set; }

        [ForeignKey(nameof(TeamId))]
        public virtual Teames.Teams Teames { get; set; }

    }
}
