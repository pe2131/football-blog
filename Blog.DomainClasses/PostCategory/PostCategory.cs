﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.DomainClasses.PostCategory
{
    public class PostCategory
    {
        [Key]
        public int PostCategoryId { get; set; }

        public int PostId { get; set; }
        [Display(Name = "Post")]
        public virtual Post.Post Post { get; set; }

        public int CategoryId { get; set; }
        [Display(Name = "Category")]
        public virtual Category.Category Category { get; set; }

    }
}