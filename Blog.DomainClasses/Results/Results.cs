﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.DomainClasses.Results
{
    public class Results
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Team1Id { get; set; }
        [Required]
        public int Team1Goals { get; set; }
        [Required]
        public int Team2Id { get; set; }
        [Required]
        public int Team2Goals { get; set; }

        public DateTime CreatedAt { get; set; }

        [ForeignKey(nameof(Team1Id))]
        public virtual Teames.Teams HomeTeams { get; set; }
        [ForeignKey(nameof(Team2Id))]
        public virtual Teames.Teams AwayTeams { get; set; }
    }
}
