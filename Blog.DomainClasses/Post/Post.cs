﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.DomainClasses.Post
{
    public class Post
    {

        [Key]
        public int PostId { get; set; }
        [Display(Name = "Author")]
        public string AuthorId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(500, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(2000, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Short Description")]
        public string ShortDesc { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Image")]
        public string ImageName { get; set; }

        public DateTime CreateDate { get; set; }

        [Display(Name = "Update Date")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "Is Publish ?")]
        public bool IsPublish { get; set; }
        [Display(Name = "Is interView?")]
        public bool IsInterView { get; set; }

        [Display(Name = "Show in Slider ?")]
        public bool ShowSlider { get; set; }

        [Display(Name = " Views")]
        public int VisitCount { get; set; }

        [Display(Name = "Short Link")]
        [MaxLength(50, ErrorMessage = "{0} can only {1} characters")]
        public string ShortKey { get; set; }
        //Relations
        public ICollection<PostCategory.PostCategory> PostCategories { get; set; }

        public ICollection<PostTag.PostTag> PostTags { get; set; }
        public ICollection<Comment.Comment> Comments { get; set; }
        public ApplicationUser Author { get; set; }

    }
}
