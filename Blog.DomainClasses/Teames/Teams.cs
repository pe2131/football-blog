﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.DomainClasses.Teames
{
    public class Teams
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Logo { get; set; }
        public int? LeagueId { get; set; }

        [ForeignKey(nameof(LeagueId))]
        public league.League League { get; set; }
    }
}
