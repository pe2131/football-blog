﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.DomainClasses.Option
{

    public class Option
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Aboute Us")]
        public string AboutUs { get; set; }
        [Display(Name = "Contact Us")]
        public string ContactUs { get; set; }
        [Display(Name = "Post Per Page")]
        public int ItemPerPage { get; set; }
        public string TopBannerImage { get; set; }
        public string TopBannerCode { get; set; }
        public string SideBannerImage1 { get; set; }
        public string SideBannerCode1 { get; set; }
        public string SideBannerImage2 { get; set; }
        public string SideBannerCode2 { get; set; }
        public string BetWeenBannerImage { get; set; }
        public string BetWeenBannerCode { get; set; }
        public string FooterBannerImage { get; set; }
        public string FooterBannerCode { get; set; }
        public string Footer { get; set; }
        public string Logo { get; set; }


    }
}
