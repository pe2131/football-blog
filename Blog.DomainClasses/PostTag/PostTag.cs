﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.DomainClasses.PostTag
{
    public class PostTag
    {
        [Key]
        public int TagId { get; set; }
        public int PostId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(250, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Tag")]
        public string Tag { get; set; }


        public Post.Post Post { get; set; }

    }
}
