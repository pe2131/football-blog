﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Blog.DomainClasses
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }

        public ICollection<Post.Post>  Posts{ get; set; }
    }
}
