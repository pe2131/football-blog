﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.DomainClasses.Comment
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }
        public int PostId { get; set; }
        public int? ParentId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(500, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(4000, ErrorMessage = "{0} can only {1} characters")]
        [Display(Name = "Text")]
        public string CommentText { get; set; }
        public string Subject { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        [Display(Name = "Publish status")]
        public bool IsPublish { get; set; }


        //Relations
        [Display(Name = "Post")]
        public virtual Post.Post Post { get; set; }
        public virtual Comment Parent { get; set; }

        public ICollection<Comment> SubComments { get; set; }

    }
}
