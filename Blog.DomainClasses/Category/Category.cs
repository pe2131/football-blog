﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.DomainClasses.Category
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        [Display(Name = "Main Cat")]
        public int? ParentId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(250, ErrorMessage = "{0} can only {1} character")]
        [Display(Name = "Name in Url")]
        public string NameInUrl { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter {0}")]
        [MaxLength(250, ErrorMessage = "{0} can only {1} character")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        //Relations
        [Display(Name = "Main Category")]
        public virtual Category Parent { get; set; }

        public ICollection<Category> SubCategories { get; set; }

        public ICollection<PostCategory.PostCategory> PostCategories { get; set; }

    }

}
