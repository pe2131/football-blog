﻿using Blog.DataLayer.Context;
using Blog.DomainClasses.Teames;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blog.Services.Services
{
    public class TeamRepository : ITeamRepository
    {
        private BlogContext _db;

        public TeamRepository(BlogContext db)
        {
            _db = db;
        }
        public void DeleteTeam(Teams Team)
        {
            _db.Teams.Remove(Team);
        }

        public List<Teams> GetAllTeams()
        {
            var Teams = _db.Teams.ToList();
            return Teams;
        }

        public Teams GetTeamById(int TeamId)
        {
            return _db.Teams.Find(TeamId);
        }
        public List<Teams> GetTeamsByLeague()
        {
            return _db.Teams.Include(i=>i.League).ToList();
        }

        public void InsertTeam(Teams Team)
        {
            _db.Teams.Add(Team);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void UpdateTeam(Teams Team)
        {
            _db.Entry(Team).State = EntityState.Modified;
        }
    }
}
