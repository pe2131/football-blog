﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Post;
using Blog.DomainClasses.PostTag;
using Blog.Services.IRepositories;
using Blog.ViewModels.Post;
using Blog.ViewModels.Search;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class PostTagRepository : IPostTagRepository
    {
        private readonly BlogContext _db;

        public PostTagRepository(BlogContext db)
        {
            _db = db;
        }
        public List<PostTag> GetAllPostTags()
        {
            return _db.PostTags.Include(t => t.Post).ToList();
        }

        public PostTag GetPostTagById(int postTagId)
        {
            return _db.PostTags.Find(postTagId);
        }
        public List<PostTag> GetPostTagByPostId(int postId)
        {
            return _db.PostTags.Where(t => t.PostId == postId).Include(t => t.Post).ToList();
        }

        public List<Post> GetPostByPostTag(string tag)
        {
            return _db.PostTags.Where(p => p.Tag.Equals(tag)).Include(p => p.Post).Select(p => p.Post).ToList();
        }
        public PostsPaginationForSearchViewModel GetPostByTagPagination(string tag, int pageId, int itemPerPage = 3)
        {
            double postCount = _db.PostTags.Where(p => p.Tag.Equals(tag) && p.Post.IsPublish == true).Include(p => p.Post).Count();

            PostsPaginationForSearchViewModel postsPagination = new PostsPaginationForSearchViewModel
            {
                CurrentPage = pageId,
                PageCount = (int)Math.Ceiling(postCount / itemPerPage),
                SearchWord = tag,
            };
            postsPagination.Posts = _db.PostTags.Where(p => p.Tag.Equals(tag) && p.Post.IsPublish == true)
                .Include(p => p.Post).Select(p => p.Post).Distinct()
                .OrderByDescending(p => p.UpdateDate)
                .Skip((pageId - 1) * itemPerPage).Take(itemPerPage).ToList();
            postsPagination.SimilarTags = _db.PostTags.Where(p => p.Tag.Contains(tag)).Select(p => p.Tag).Distinct().ToList();
            return postsPagination;
        }
        public void InsertPostTag(PostTag postTag)
        {
            _db.PostTags.Add(postTag);
        }

        public void UpdatePostTag(PostTag postTag)
        {
            _db.Entry(postTag).State = EntityState.Modified;
        }

        public void DeletePostTag(PostTag postTag)
        {
            _db.Entry(postTag).State = EntityState.Deleted;
        }

        public void DeletePostTag(int postTagId)
        {
            var postTag = GetPostTagById(postTagId);
            if (postTag != null)
            {
                DeletePostTag(postTag);
            }
        }
        public bool PostTagExists(int id)
        {
            return _db.PostTags.Any(e => e.TagId == id);
        }
        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
