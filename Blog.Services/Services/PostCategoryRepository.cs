﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.PostCategory;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class PostCategoryRepository : IPostCategoryRepository
    {
        private readonly BlogContext _db;

        public PostCategoryRepository(BlogContext db)
        {
            _db = db;
        }
        public List<PostCategory> GetAllPostCategories()
        {
            return _db.PostCategories.Include(c => c.Category).Include(c => c.Post).ToList();
        }

        public PostCategory GetPostCategoryById(int postCategoryId)
        {
            return _db.PostCategories.Find(postCategoryId);
        }

        public List<PostCategory> GetPostCategoriesByPostId(int postId)
        {
            return _db.PostCategories.Where(pc => pc.PostId == postId).Include(pc => pc.Category).Include(pc => pc.Post).ToList();
        }

        public List<Category> GetCategoriesFromPostCategoriesByPostId(int postId)
        {
            return _db.PostCategories.Where(pc => pc.PostId == postId).Include(pc => pc.Category).Select(pc => pc.Category).Distinct().ToList();
        }

        public void InsertPostCategory(PostCategory category)
        {
            _db.PostCategories.Add(category);
        }

        public void UpdatePostCategory(PostCategory postCategory)
        {
            _db.Entry(postCategory).State = EntityState.Modified;
        }

        public void DeletePostCategory(PostCategory postCategory)
        {
            _db.Entry(postCategory).State = EntityState.Deleted;
        }

        public void DeletePostCategory(int postCategoryId)
        {
            var postCategory = GetPostCategoryById(postCategoryId);
            if (postCategory != null)
            {
                DeletePostCategory(postCategory);
            }
        }
        public bool PostCategoryExists(int id)
        {
            return _db.PostCategories.Any(e => e.PostId == id);
        }
        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
