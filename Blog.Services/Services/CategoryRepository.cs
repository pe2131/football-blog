﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Category;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class CategoryRepository : ICategoryRepository
    {
        private BlogContext _db;

        public CategoryRepository(BlogContext db)
        {
            _db = db;
        }
        public List<Category> GetAllCategories()
        {
            return _db.Categories.ToList();
        }

        public Category GetCategoryById(int categoryId)
        {
            return _db.Categories.Find(categoryId);
        }
        public List<Category> GetCategoryForShow()
        {
            return _db.Categories.Where(p=>p.ParentId==null).ToList();
        }
        public void InsertCategory(Category category)
        {
             _db.Categories.Add(category);
        }

        public void UpdateCategory(Category category)
        {
             _db.Entry(category).State = EntityState.Modified;
        }

        public void DeleteCategory(Category category)
        {
             _db.Entry(category).State = EntityState.Deleted;
        }

        public void DeleteCategory(int categoryId)
        {
            var category =  GetCategoryById(categoryId);
            if (category != null)
            {
                 DeleteCategory(category);
            }
        }
        public bool CategoryExists(int id)
        {
            return _db.Categories.Any(e => e.CategoryId == id);
        }
        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
