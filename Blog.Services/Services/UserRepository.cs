﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blog.DataLayer.Context;
using Blog.DomainClasses;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class UserRepository : IUserRepository
    {
        private BlogContext _db;

        public UserRepository(BlogContext db)
        {
            _db = db;
        }
        public List<ApplicationUser> GetAllUsers()
        {
            return _db.Users.Include(u => u.Posts).ToList();
        }

        public ApplicationUser GetUserById(string userId)
        {
            return _db.Users.FirstOrDefault(u => u.Id == userId);
        }
        public ApplicationUser GetUserByName(string userName)
        {
            return _db.Users.FirstOrDefault(u => u.UserName == userName);
        }

        public void UserActiveStatusChanger(string id, bool isActive)
        {
            var post = GetUserById(id);
            post.IsActive = isActive;
            Save();
        }
        public void InsertUser(ApplicationUser user)
        {
            _db.Users.Add(user);
        }

        public void UpdateUser(ApplicationUser user)
        {
            _db.Entry(user).State = EntityState.Modified;
        }

        public void DeleteUser(ApplicationUser user)
        {
            _db.Entry(user).State = EntityState.Deleted;
        }

        public void DeleteUser(string userId)
        {
            var user = GetUserById(userId);
            if (user != null)
            {
                DeleteUser(user);
            }
        }
        public bool UserExists(string id)
        {
            return _db.Users.Any(e => e.Id == id);
        }
        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
