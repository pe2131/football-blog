﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Slider;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class SliderRepository : ISliderRepository
    {
        private BlogContext _db;

        public SliderRepository(BlogContext db)
        {
            _db = db;
        }
        public List<Slider> GetAllSliders()
        {
            return _db.Sliders.OrderByDescending(s=>s.CreateDate).ToList();
        }

        public Slider GetSliderById(int sliderId)
        {
            return _db.Sliders.Find(sliderId);
        }
        public List<Slider> GetSliderForShow()
        {
            return _db.Sliders.Where(s => s.IsPublish == true).ToList();
        }
        public void SliderPublishStatusChanger(int id, bool isPublish)
        {
            var post = GetSliderById(id);
            post.IsPublish = isPublish;
            Save();
        }
        public void InsertSlider(Slider slider)
        {
            _db.Sliders.Add(slider);
        }

        public void UpdateSlider(Slider slider)
        {
            _db.Entry(slider).State = EntityState.Modified;
        }

        public void DeleteSlider(Slider slider)
        {
            _db.Entry(slider).State = EntityState.Deleted;
        }

        public void DeleteSlider(int sliderId)
        {
            var slider = GetSliderById(sliderId);
            if (slider != null)
            {
                DeleteSlider(slider);
            }
        }
        public bool SliderExists(int id)
        {
            return _db.Sliders.Any(e => e.SliderId == id);
        }

        public void IncreaseVisitCount(int sliderId)
        {
            var slider = GetSliderById(sliderId);
            slider.VisitCount += 1;
            //UpdatePost(post);
            Save();
        }

        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
