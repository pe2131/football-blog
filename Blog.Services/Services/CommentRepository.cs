﻿using System.Collections.Generic;
using System.Linq;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Comment;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class CommentRepository : ICommentRepository
    {
        private BlogContext _db;

        public CommentRepository(BlogContext db)
        {
            _db = db;
        }
        public List<Comment> GetAllComments()
        {
            return _db.Comments.ToList();
        }

        public List<Comment> GetAllCommentsForParent(int parentId)
        {
            return _db.Comments.Where(c => c.ParentId == parentId).Include(p=>p.Post).OrderByDescending(c => c.CreateDate).ToList();
        }

        public List<Comment> GetAllParentComments()
        {
            return _db.Comments.Where(c => c.ParentId == null).Include(c=>c.Post).OrderByDescending(c => c.CreateDate).ToList();
        }

        public Comment GetCommentById(int commentId)
        {
            return _db.Comments.Find(commentId);
        }
        public List<Comment> GetCommentByPostId(int postId)
        {
            return _db.Comments.Where(p => p.PostId == postId && p.IsPublish == true).ToList();
        }
        public void CommentPublishStatusChanger(int id, bool isPublish)
        {
            var post = GetCommentById(id);
            post.IsPublish = isPublish;
            Save();
        }
        public void InsertComment(Comment comment)
        {
            _db.Comments.Add(comment);
        }

        public void UpdateComment(Comment comment)
        {
            _db.Entry(comment).State = EntityState.Modified;
        }

        public void DeleteComment(Comment comment)
        {
            var subComments=_db.Comments.Where(c => c.ParentId == comment.CommentId).Include(c=>c.SubComments).ToList();
            foreach (var item in subComments)
            {
                DeleteComment(item);
                Save();
            }

            _db.Entry(comment).State = EntityState.Deleted;
        }

        public void DeleteComment(int commentId)
        {
            var comment = GetCommentById(commentId);
            if (comment != null)
            {
                DeleteComment(comment);
            }
        }
        public bool CommentExists(int id)
        {
            return _db.Comments.Any(e => e.CommentId == id);
        }
        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
