﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Option;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class OptionRepository : IOptionRepository
    {
        private BlogContext _db;

        public OptionRepository(BlogContext db)
        {
            _db = db;
        }
        public Option GetOption()
        {
            return _db.Options.FirstOrDefault();
        }
        public Option GetOptionById(int optionId)
        {
            return _db.Options.Find(optionId);
        }
        public void InsertOption(Option option)
        {
            _db.Options.Add(option);
        }

        public void UpdateOption(Option option)
        {
            _db.Entry(option).State = EntityState.Modified;
        }

        public void DeleteOption(Option option)
        {
            _db.Entry(option).State = EntityState.Deleted;
        }

        public void DeleteOption(int optionId)
        {
            var option = GetOptionById(optionId);
            if (option != null)
            {
                DeleteOption(option);
            }
        }
        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
