﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Post;
using Blog.DomainClasses.PostCategory;
using Blog.Services.IRepositories;
using Blog.ViewModels.Post;
using Blog.ViewModels.Search;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class PostRepository : IPostRepository
    {
        private readonly BlogContext _db;

        public PostRepository(BlogContext db)
        {
            _db = db;
        }

        public List<Post> GetAllPosts()
        {
            return _db.Posts.OrderByDescending(p => p.UpdateDate).Include(p => p.Author).ToList();
        }

        public Post GetPostById(int postId)
        {
            return _db.Posts.Where(p => p.PostId == postId).Include(p => p.Author).FirstOrDefault();
        }
        public Post GetNextPost(int postId)
        {
            return _db.Posts.Where(p => p.PostId > postId).Include(p => p.Author).OrderBy(q => q.PostId).FirstOrDefault();
        }
        public Post GetPreiviosPost(int postId)
        {
            return _db.Posts.Where(p => p.PostId < postId).Include(p => p.Author).OrderByDescending(q => q.PostId).FirstOrDefault();
        }
        public Post GetPostByShortKey(string shortkey)
        {
            return _db.Posts.FirstOrDefault(p => p.ShortKey.Equals(shortkey));
        }

        public PostsPaginationViewModel GetPostForIndexPagination(int pageId, int itemPerPage = 3)
        {
            double allPosts = _db.Posts.Count(p => p.IsPublish == true);
            PostsPaginationViewModel postsPagination = new PostsPaginationViewModel
            {
                Posts = _db.Posts.Where(p => p.IsPublish == true).Include(q => q.Comments).OrderByDescending(p => p.UpdateDate).Skip((pageId - 1) * itemPerPage).Take(itemPerPage).ToList(),
                CurrentPage = pageId,
                PageCount = (int)Math.Ceiling(allPosts / itemPerPage)
            };
            return postsPagination;
        }

        public async Task<int> GetTotalPagePostsAsync(string name)
        {
            var Posts = await _db.PostCategories.Include(pc => pc.Category).Where(pc => pc.Category.NameInUrl == name)
            .Include(pc => pc.Post).ThenInclude(c => c.Comments).Select(pc => pc.Post).Where(q => q.IsPublish == true && !q.ShowSlider).ToListAsync();
            IList<Post> withoutCat = await _db.Posts.Where(q => q.IsPublish == true && q.PostCategories.Count == 0 && !q.ShowSlider).Include(q => q.PostCategories).Include(q => q.Comments).ToListAsync();
            Posts.AddRange(withoutCat);
            int allPosts = Posts.Count(p => p.IsPublish == true);
            return allPosts;
        }

        public async Task<List<Post>> GetPostForIndexInfiniteScrollPagination(string name, int pageId, int itemPerPage)
        {
            var Posts = await _db.PostCategories.Include(pc => pc.Category).Where(pc => pc.Category.NameInUrl == name)
               .Include(pc => pc.Post).ThenInclude(c => c.Comments).Select(pc => pc.Post).Where(q => q.IsPublish == true && !q.ShowSlider).ToListAsync();
            IList<Post> withoutCat = await _db.Posts.Where(q => q.IsPublish == true && q.PostCategories.Count == 0 && !q.ShowSlider).Include(q => q.PostCategories).Include(q => q.Comments).ToListAsync();
            Posts.AddRange(withoutCat);
            double allPosts = Posts.Count(p => p.IsPublish == true);
            var query = Posts.OrderByDescending(d => d.UpdateDate).Skip((pageId - 1) * itemPerPage).Take(itemPerPage);
            return query.ToList();
        }

        public async Task<PostsPaginationViewModel> GetPostForIndexPagination(string name, int pageId, int itemPerPage = 3)
        {
            var Posts = await _db.PostCategories.Include(pc => pc.Category).Where(pc => pc.Category.NameInUrl == name)
               .Include(pc => pc.Post).ThenInclude(c => c.Comments).Select(pc => pc.Post).Where(q => q.IsPublish == true).ToListAsync();
            IList<Post> withoutCat = await _db.Posts.Where(q => q.IsPublish == true && q.PostCategories.Count == 0).Include(q => q.PostCategories).Include(q => q.Comments).ToListAsync();
            Posts.AddRange(withoutCat);
            double allPosts = Posts.Count(p => p.IsPublish == true);
            PostsPaginationViewModel postsPagination = new PostsPaginationViewModel
            {
                Posts = Posts.OrderByDescending(p => p.UpdateDate).Skip((pageId - 1) * itemPerPage).Take(itemPerPage).ToList(),
                CurrentPage = pageId,
                PageCount = (int)Math.Ceiling(allPosts / itemPerPage)
            };
            return postsPagination;
        }
        public List<Post> GetPostsById(int id)
        {
            return _db.Posts.Where(p => p.PostId == id).ToList();
        }

        public PostsPaginationForSearchViewModel GetPostByWordPagination(string word, int pageId, int itemPerPage = 3)
        {

            double postCount = _db.Posts.Where(p => p.Title.Contains(word) || p.Description.Contains(word) || p.ShortDesc.Contains(word)).Count(p => p.IsPublish == true);

            PostsPaginationForSearchViewModel postsPagination = new PostsPaginationForSearchViewModel
            {
                CurrentPage = pageId,
                PageCount = (int)Math.Ceiling(postCount / itemPerPage),
                SearchWord = word,
            };
            postsPagination.Posts = _db.Posts.Where(p => p.Title.Contains(word) || p.Description.Contains(word) || p.ShortDesc.Contains(word))
                .Where(p => p.IsPublish == true)
                .Distinct()
                .OrderByDescending(p => p.UpdateDate)
                .Skip((pageId - 1) * itemPerPage).Take(itemPerPage).ToList();
            postsPagination.SimilarTags = _db.PostTags.Where(p => p.Tag.Contains(word)).Select(p => p.Tag).Distinct().ToList();
            return postsPagination;
        }

        public PostsPaginationForSearchViewModel GetPostByCategoryPagination(string name, int pageId, int itemPerPage = 3)
        {
            //  double postCount =_db.Categories.Where(c=>c.NameInUrl==name).Include(c=>c.PostCategories).Include(c=>c.PostCategories.)
            double postCount = _db.PostCategories.Include(pc => pc.Category).Where(pc => pc.Category.NameInUrl == name).Include(pc => pc.Post).Select(pc => pc.Post)
                .Where(p => p.IsPublish == true).Distinct().Count();
            PostsPaginationForSearchViewModel postsPagination = new PostsPaginationForSearchViewModel
            {
                CurrentPage = pageId,
                PageCount = (int)Math.Ceiling(postCount / itemPerPage),
                SearchWord = name,
            };
            postsPagination.Posts = _db.PostCategories.Include(pc => pc.Category).Where(pc => pc.Category.NameInUrl == name)
                .Include(pc => pc.Post).ThenInclude(c => c.Comments).Select(pc => pc.Post)
                .Where(p => p.IsPublish == true).Distinct()
                .OrderByDescending(p => p.UpdateDate)
                .Skip((pageId - 1) * itemPerPage).Take(itemPerPage).ToList();
            postsPagination.SimilarTags = _db.PostTags.Where(p => p.Tag.Contains(name)).Select(p => p.Tag).Distinct().ToList();
            return postsPagination;
        }
        public PostsPaginationForSearchViewModel GetPostExeptCategoryPagination(string name, int pageId, int itemPerPage = 3)
        {
            //  double postCount =_db.Categories.Where(c=>c.NameInUrl==name).Include(c=>c.PostCategories).Include(c=>c.PostCategories.)
            double postCount = _db.PostCategories.Include(pc => pc.Category).Where(pc => pc.Category.NameInUrl != name).Include(pc => pc.Post).Select(pc => pc.Post)
                .Where(p => p.IsPublish == true).Distinct().Count();
            PostsPaginationForSearchViewModel postsPagination = new PostsPaginationForSearchViewModel
            {
                CurrentPage = pageId,
                PageCount = (int)Math.Ceiling(postCount / itemPerPage),
                SearchWord = name,
            };
            postsPagination.Posts = _db.PostCategories.Include(pc => pc.Category).Where(pc => pc.Category.NameInUrl != name)
                .Include(pc => pc.Post).ThenInclude(c => c.Comments).Select(pc => pc.Post)
                .Where(p => p.IsPublish == true).Distinct()
                .OrderByDescending(p => p.UpdateDate)
                .Skip((pageId - 1) * itemPerPage).Take(itemPerPage).ToList();
            postsPagination.SimilarTags = _db.PostTags.Where(p => p.Tag.Contains(name)).Select(p => p.Tag).Distinct().ToList();
            return postsPagination;
        }
        public PostsPaginationForSearchViewModel GetPostByAuthorPagination(string name, int pageId, int itemPerPage = 3)
        {
            var userId = _db.Users.FirstOrDefault(u => u.Name.Contains(name)).Id;

            double postCount = _db.Posts.Where(p => p.AuthorId == userId && p.IsPublish == true).Distinct().Count();
            PostsPaginationForSearchViewModel postsPagination = new PostsPaginationForSearchViewModel
            {
                CurrentPage = pageId,
                PageCount = (int)Math.Ceiling(postCount / itemPerPage),
                SearchWord = name,
            };

            postsPagination.Posts = _db.Posts.Where(p => p.AuthorId == userId && p.IsPublish == true).Distinct()
                .OrderByDescending(p => p.UpdateDate)
                .Skip((pageId - 1) * itemPerPage).Take(itemPerPage).ToList();
            postsPagination.SimilarTags = _db.PostTags.Where(p => p.Tag.Contains(name)).Select(p => p.Tag).Distinct().ToList();
            return postsPagination;
        }
        public void PostSliderStatusChanger(int id, bool isPublish)
        {
            var post = GetPostById(id);
            post.ShowSlider = isPublish;
            Save();
        }
        public List<Post> GetSliderForShow()
        {
            return _db.Posts.Where(s => s.ShowSlider == true).ToList();
        }
        public void PostPublishStatusChanger(int id, bool isPublish)
        {
            var post = GetPostById(id);
            post.IsPublish = isPublish;
            Save();
        }
        public void InsertPost(Post post)
        {
            _db.Posts.Add(post);
        }

        public void UpdatePost(Post post)
        {
            _db.Entry(post).State = EntityState.Modified;
        }

        public void DeletePost(Post post)
        {
            _db.Entry(post).State = EntityState.Deleted;
        }

        public void DeletePost(int postId)
        {
            var post = GetPostById(postId);
            if (post != null)
            {
                DeletePost(post);
            }
        }
        public bool PostExists(int id)
        {
            return _db.Posts.Any(e => e.PostId == id);
        }

        public bool ShortKeyIsValid(string shortLink)
        {
            if (!_db.Posts.Any(p => p.ShortKey.Equals(shortLink)))
            {
                return true;
            }

            return false;
        }

        public void IncreaseVisitCount(int postId)
        {
            var post = GetPostById(postId);
            post.VisitCount += 1;
            //UpdatePost(post);
            Save();
        }

        public void Save()
        {
            _db.SaveChanges();
        }


    }
}
