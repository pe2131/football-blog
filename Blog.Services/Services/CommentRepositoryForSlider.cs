﻿using System.Collections.Generic;
using System.Linq;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Comment;
using Blog.DomainClasses.commentForSlider;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blog.Services.Services
{
    public class CommentRepositoryForSlider : ICommentRepositoryForSlider
    {
        private BlogContext _db;

        public CommentRepositoryForSlider(BlogContext db)
        {
            _db = db;
        }
        public List<CommentForSlider> GetAllComments()
        {
            return _db.CommentsForSlider.ToList();
        }

        public List<CommentForSlider> GetAllCommentsForParent(int parentId)
        {
            return _db.CommentsForSlider.Where(c => c.ParentId == parentId).Include(p=>p.Slider).OrderByDescending(c => c.CreateDate).ToList();
        }

        public List<CommentForSlider> GetAllParentComments()
        {
            return _db.CommentsForSlider.Where(c => c.ParentId == null).Include(c=>c.Slider).OrderByDescending(c => c.CreateDate).ToList();
        }

        public CommentForSlider GetCommentById(int commentId)
        {
            return _db.CommentsForSlider.Find(commentId);
        }
        public List<CommentForSlider> GetCommentBySliderId(int SliderId)
        {
            return _db.CommentsForSlider.Where(p => p.SliderId == SliderId && p.IsPublish == true).ToList();
        }
        public void CommentPublishStatusChanger(int id, bool isPublish)
        {
            var post = GetCommentById(id);
            post.IsPublish = isPublish;
            Save();
        }
        public void InsertComment(CommentForSlider comment)
        {
            _db.CommentsForSlider.Add(comment);
        }

        public void UpdateComment(CommentForSlider comment)
        {
            _db.Entry(comment).State = EntityState.Modified;
        }

        public void DeleteComment(CommentForSlider comment)
        {
            var subComments=_db.CommentsForSlider.Where(c => c.ParentId == comment.CommentId).Include(c=>c.SubComments).ToList();
            foreach (var item in subComments)
            {
                DeleteComment(item);
                Save();
            }

            _db.Entry(comment).State = EntityState.Deleted;
        }

        public void DeleteComment(int commentId)
        {
            var comment = GetCommentById(commentId);
            if (comment != null)
            {
                DeleteComment(comment);
            }
        }
        public bool CommentExists(int id)
        {
            return _db.CommentsForSlider.Any(e => e.CommentId == id);
        }
        public void Save()
        {
            _db.SaveChanges();
        }

    }
}
