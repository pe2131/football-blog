﻿using Blog.DataLayer.Context;
using Blog.DomainClasses.Ranking;
using Blog.Services.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blog.Services.Services
{
    public class RankingRepository : IRankingRepository
    {
        private BlogContext _db;

        public RankingRepository(BlogContext db)
        {
            _db = db;
        }
        public void DeleteRank(Ranking Rank)
        {
            _db.Rankings.Remove(Rank);
        }

        public List<Ranking> GetAllRank()
        {
            var Teams = _db.Rankings.ToList();
            return Teams;
        }

        public Ranking GetTeamById(int RankId)
        {
            return _db.Rankings.Find(RankId);
        }

        public void InsertRank(Ranking Rank)
        {
            _db.Rankings.Add(Rank);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void UpdateRank(Ranking Rank)
        {
            _db.Entry(Rank).State = EntityState.Modified;
        }
    }
}
