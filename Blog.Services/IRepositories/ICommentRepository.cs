﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Comment;
using Blog.DomainClasses.Post;

namespace Blog.Services.IRepositories
{
    public interface ICommentRepository
    {
        List<Comment> GetAllComments();
        List<Comment> GetAllCommentsForParent(int parentId);
        List<Comment> GetAllParentComments();
        Comment GetCommentById(int commentId);
        List<Comment> GetCommentByPostId(int postId);
        void CommentPublishStatusChanger(int id, bool isPublish);
        void InsertComment(Comment comment);
        void UpdateComment(Comment comment);
        void DeleteComment(Comment comment);
        void DeleteComment(int comment);
        bool CommentExists(int commentId);
        void Save();
    }
}
