﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Post;
using Blog.DomainClasses.PostCategory;
using Blog.ViewModels.Post;
using Blog.ViewModels.Search;

namespace Blog.Services.IRepositories
{
    public interface IPostRepository
    {
        List<Post> GetAllPosts();
        Post GetPostById(int postId);
        Post GetNextPost(int postId);
        Post GetPreiviosPost(int postId);
        Post GetPostByShortKey(string shortkey);
        PostsPaginationViewModel GetPostForIndexPagination(int pageId, int itemPerPage = 3);
        Task<int> GetTotalPagePostsAsync(string name);
        Task<List<Post>> GetPostForIndexInfiniteScrollPagination(string name, int pageId, int itemPerPage);
        Task<PostsPaginationViewModel> GetPostForIndexPagination(string cat, int pageId, int itemPerPage = 3);
        List<Post> GetPostsById(int id);
        void PostSliderStatusChanger(int id, bool isPublish);
        List<Post> GetSliderForShow();
        PostsPaginationForSearchViewModel GetPostByWordPagination(string word, int pageId, int itemPerPage = 3);
        PostsPaginationForSearchViewModel GetPostByCategoryPagination(string name, int pageId, int itemPerPage = 3);
        PostsPaginationForSearchViewModel GetPostExeptCategoryPagination(string name, int pageId, int itemPerPage = 3);

        PostsPaginationForSearchViewModel GetPostByAuthorPagination(string name, int pageId, int itemPerPage = 3);
        void PostPublishStatusChanger(int id, bool isPublish);
        void InsertPost(Post post);
        void UpdatePost(Post post);
        void DeletePost(Post post);
        void DeletePost(int post);
        bool PostExists(int postId);
        bool ShortKeyIsValid(string shortLink);
        void IncreaseVisitCount(int postId);
        void Save();
    }
}
