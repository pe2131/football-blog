﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.PostTag;
using Blog.DomainClasses.Post;
using Blog.ViewModels.Search;

namespace Blog.Services.IRepositories
{
    public interface IPostTagRepository
    {
        List<PostTag> GetAllPostTags();
        PostTag GetPostTagById(int postTagId);
        List<PostTag> GetPostTagByPostId(int postId);
        List<Post> GetPostByPostTag(string tag);
        PostsPaginationForSearchViewModel GetPostByTagPagination(string tag, int pageId, int itemPerPage = 3);
        void InsertPostTag(PostTag postTag);
        void UpdatePostTag(PostTag postTag);
        void DeletePostTag(PostTag postTag);
        void DeletePostTag(int postTag);
        bool PostTagExists(int postTagId);
        void Save();
    }
}
