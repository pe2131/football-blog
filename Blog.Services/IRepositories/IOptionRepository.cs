﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.DomainClasses;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Option;
using Blog.DomainClasses.Slider;

namespace Blog.Services.IRepositories
{
    public interface IOptionRepository
    {
        Option GetOption();
        void InsertOption(Option option);
        void UpdateOption(Option option);
        void DeleteOption(Option option);
        void DeleteOption(int optionId);
        void Save();
    }
}
