﻿using Blog.DomainClasses.Ranking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Services.IRepositories
{
   public interface IRankingRepository
    {
        Ranking GetTeamById(int RankId);
        void InsertRank(Ranking Rank);
        void UpdateRank(Ranking Rank);
        void DeleteRank(Ranking Rank);
        List<Ranking> GetAllRank();
        void Save();
    }
}
