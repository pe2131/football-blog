﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Slider;

namespace Blog.Services.IRepositories
{
    public interface ISliderRepository
    {
        List<Slider> GetAllSliders();
        Slider GetSliderById(int sliderId);
        List<Slider> GetSliderForShow();
        void SliderPublishStatusChanger(int id, bool isPublish);
        void InsertSlider(Slider slider);
        void UpdateSlider(Slider slider);
        void DeleteSlider(Slider slider);
        void DeleteSlider(int slider);
        bool SliderExists(int sliderId);
        void IncreaseVisitCount(int sliderId);
        void Save();
    }
}
