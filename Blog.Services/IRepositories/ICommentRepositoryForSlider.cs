﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Comment;
using Blog.DomainClasses.commentForSlider;
using Blog.DomainClasses.Post;

namespace Blog.Services.IRepositories
{
    public interface ICommentRepositoryForSlider
    {
        List<CommentForSlider> GetAllComments();
        List<CommentForSlider> GetAllCommentsForParent(int parentId);
        List<CommentForSlider> GetAllParentComments();
        CommentForSlider GetCommentById(int commentId);
        List<CommentForSlider> GetCommentBySliderId(int postId);
        void CommentPublishStatusChanger(int id, bool isPublish);
        void InsertComment(CommentForSlider comment);
        void UpdateComment(CommentForSlider comment);
        void DeleteComment(CommentForSlider comment);
        void DeleteComment(int CommentForSlider);
        bool CommentExists(int CommentForSlider);
        void Save();
    }
}
