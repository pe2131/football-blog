﻿using Blog.DomainClasses.Teames;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Services.IRepositories
{
   public interface ITeamRepository
    {
        Teams GetTeamById(int TeamId);
        void InsertTeam(Teams Team);
        void UpdateTeam(Teams Team);
        void DeleteTeam(Teams Team);
        List<Teams> GetAllTeams();
        List<Teams> GetTeamsByLeague();
        void Save();
    }
}
