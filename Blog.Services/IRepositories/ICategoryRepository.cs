﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.DomainClasses.Category;

namespace Blog.Services.IRepositories
{
    public interface ICategoryRepository
    {
        List<Category> GetAllCategories();
        Category GetCategoryById(int categoryId);
        List<Category> GetCategoryForShow();
        void InsertCategory(Category category);
        void UpdateCategory(Category category);
        void DeleteCategory(Category category);
        void DeleteCategory(int categoryId);
        bool CategoryExists(int id);
        void Save();
    }
}
