﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Post;
using Blog.DomainClasses.PostCategory;

namespace Blog.Services.IRepositories
{
    public interface IPostCategoryRepository
    {
        List<PostCategory> GetAllPostCategories();
        PostCategory GetPostCategoryById(int postCategoryId);
        List<PostCategory> GetPostCategoriesByPostId(int postId);
        List<Category> GetCategoriesFromPostCategoriesByPostId(int postId);
        void InsertPostCategory(PostCategory postCategory);
        void UpdatePostCategory(PostCategory postCategory);
        void DeletePostCategory(PostCategory postCategory);
        void DeletePostCategory(int postCategory);
        bool PostCategoryExists(int postCategoryId);
        void Save();
    }
}
