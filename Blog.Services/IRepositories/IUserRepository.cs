﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.DomainClasses;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Slider;

namespace Blog.Services.IRepositories
{
    public interface IUserRepository
    {
        List<ApplicationUser> GetAllUsers();
        ApplicationUser GetUserById(string userId);
        ApplicationUser GetUserByName(string userName);
        void UserActiveStatusChanger(string id, bool isActive);
        void InsertUser(ApplicationUser user);
        void UpdateUser(ApplicationUser user);
        void DeleteUser(ApplicationUser user);
        void DeleteUser(string userId);
        bool UserExists(string userId);
        void Save();
    }
}
