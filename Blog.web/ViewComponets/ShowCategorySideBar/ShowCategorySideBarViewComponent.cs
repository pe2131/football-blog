﻿using System.Threading.Tasks;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.ViewComponets.ShowCategorySideBar
{
    public class ShowCategorySideBarViewComponent:ViewComponent
    {
        public ICategoryRepository _categoryRepository;

        public ShowCategorySideBarViewComponent(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public  Task<IViewComponentResult> InvokeAsync()
        {
            return  Task.FromResult((IViewComponentResult)View("_CategorySideBar", _categoryRepository.GetCategoryForShow()));
        }
    }
}
