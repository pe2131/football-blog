﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.DataLayer.Context;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.ViewComponets.ShowAuthors
{
    public class ShowAuthorsViewComponent:ViewComponent
    {
        public readonly BlogContext _blogContext;

        public ShowAuthorsViewComponent(BlogContext blogContext)
        {
            _blogContext = blogContext;
        }
        public Task<IViewComponentResult> InvokeAsync()
        {
            return Task.FromResult((IViewComponentResult)View("_ShowAuthors", _blogContext.Users.Where(p=>p.IsActive).ToList()));
        }
    }
}
