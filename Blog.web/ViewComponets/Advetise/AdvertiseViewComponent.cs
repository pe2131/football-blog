﻿using Blog.DataLayer.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.web.ViewComponets.Advetise
{
    public class AdvertiseViewComponent:ViewComponent
    {
        private readonly BlogContext _context;
        public AdvertiseViewComponent(BlogContext contex)
        {
            _context = contex;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            try
            {
                return await Task.FromResult((IViewComponentResult)View("P_Advertise", await _context.Options.FirstOrDefaultAsync()));
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
