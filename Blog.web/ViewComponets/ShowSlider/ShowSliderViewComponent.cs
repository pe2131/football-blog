﻿using System.Linq;
using System.Threading.Tasks;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.ViewComponets.ShowSlider
{
    public class ShowSliderViewComponent : ViewComponent
    {
        public readonly IPostRepository _postRepository;

        public ShowSliderViewComponent(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }
        public  Task<IViewComponentResult> InvokeAsync()
        {
            return  Task.FromResult((IViewComponentResult)View("_ShowSlider", _postRepository.GetSliderForShow().OrderByDescending(q=>q.PostId).Take(4)));
        }
    }
}
