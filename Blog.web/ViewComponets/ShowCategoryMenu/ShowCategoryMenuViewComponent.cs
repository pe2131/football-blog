﻿using System.Threading.Tasks;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.ViewComponets.ShowCategoryMenu
{
    public class ShowCategoryMenuViewComponent : ViewComponent
    {
        public ICategoryRepository _categoryRepository;

        public ShowCategoryMenuViewComponent(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public Task<IViewComponentResult> InvokeAsync()
        {
            return Task.FromResult((IViewComponentResult)View("_CategoryMenu", _categoryRepository.GetCategoryForShow()));
        }
    }
}
