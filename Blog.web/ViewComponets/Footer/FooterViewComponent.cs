﻿using Blog.DataLayer.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.web.ViewComponets.Footer
{
    public class FooterViewComponent:ViewComponent
    {
        private readonly BlogContext _context;
        public FooterViewComponent(BlogContext contex)
        {
            _context = contex;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var options = _context.Options.FirstOrDefault();
            return await Task.FromResult((IViewComponentResult)View("P_Footer", options));
        }
    }
}
