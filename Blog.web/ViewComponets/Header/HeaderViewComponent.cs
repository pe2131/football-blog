﻿using Blog.DataLayer.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.web.ViewComponets.Header
{
    public class HeaderViewComponent : ViewComponent
    {
        private readonly BlogContext _context;
        public HeaderViewComponent(BlogContext contex)
        {
            _context = contex;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var options = _context.Options.FirstOrDefault();
            return await Task.FromResult((IViewComponentResult)View("P_Header", options));
        }
    }
}
