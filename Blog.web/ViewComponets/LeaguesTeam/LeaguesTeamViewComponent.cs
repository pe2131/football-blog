﻿using Blog.DataLayer.Context;
using Blog.web.Models.TablesViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.web.ViewComponets.LeaguesTeam
{
    public class LeaguesTeamViewComponent: ViewComponent
    {
        private readonly BlogContext _Context;
        public LeaguesTeamViewComponent(BlogContext Context)
        {
            _Context = Context;
        }
        public async Task<IViewComponentResult> InvokeAsync(int league = 0)
        {
            try
            {
                TablesViewModel model = new TablesViewModel();
                model.leagues = _Context.leagues;
                if (league == 0)
                {
                    model.Rankings = _Context.Rankings.OrderByDescending(o => o.GD).Include(q => q.Teames).ThenInclude(q => q.League).Where(q => q.Teames.LeagueId == model.leagues.FirstOrDefault().Id).ToList();
                    model.Results = _Context.Results.OrderByDescending(o => o.CreatedAt).Include(q => q.AwayTeams).Include(q => q.HomeTeams).ToList();
                    model.Schedules = _Context.schedules.OrderBy(o => o.MatchDay).Include(q => q.AwayTeams).Include(q => q.HomeTeams).ToList();
                }
                else
                {
                    model.Rankings = _Context.Rankings.OrderByDescending(o => o.GD).Include(q => q.Teames).ThenInclude(q => q.League).Where(q => q.Teames.LeagueId == league).ToList();
                    model.Results = _Context.Results.OrderByDescending(o => o.CreatedAt).Include(q => q.AwayTeams).Include(q => q.HomeTeams).ThenInclude(i=>i.League).Where(q=>q.HomeTeams.LeagueId==league).ToList();
                    model.Schedules = _Context.schedules.OrderBy(o => o.MatchDay).Include(q => q.AwayTeams).Include(q => q.HomeTeams).ThenInclude(i => i.League).Where(q => q.HomeTeams.LeagueId == league).ToList();
                }
               
                model.SiteSetting = _Context.Options.FirstOrDefault();
                return await Task.FromResult((IViewComponentResult)View("P_LeagueTeam", model));
            }
            catch (Exception e)
            {
                throw e;
            }
          
        }
    }
}
