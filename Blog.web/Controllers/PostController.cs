﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Comment;
using Blog.DomainClasses.Post;
using Blog.DomainClasses.PostCategory;
using Blog.Services.IRepositories;
using Blog.Utilities.String;
using Blog.ViewModels.Post;
using Microsoft.AspNetCore.Http.Headers;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IPostTagRepository _postTagRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly IPostCategoryRepository _postCategoryRepository;


        public PostController(IPostRepository postRepository, ICategoryRepository categoryRepository, IPostTagRepository postTagRepository, ICommentRepository commentRepository, IPostCategoryRepository postCategoryRepository)
        {
            _postRepository = postRepository;
            _categoryRepository = categoryRepository;
            _postTagRepository = postTagRepository;
            _commentRepository = commentRepository;
            _postCategoryRepository = postCategoryRepository;
        }

        [Route("ShowPost/{id}/{name?}")]
        public IActionResult Index(int id, string name, bool? CommentStatus = null) //نمایش پست  
        {
            Post post = _postRepository.GetPostById(id); //پیدا کردن پست  و دریافت آن از Repository

            if (post == null)
            {
                return NotFound();
            }
            _postRepository.IncreaseVisitCount(id);

            ViewPostViewModel viewPost = new ViewPostViewModel()
            {
                Post = post,
                Categories = _postCategoryRepository.GetCategoriesFromPostCategoriesByPostId(id),//دسته بندی مربوطه
                PostTags = _postTagRepository.GetPostTagByPostId(id),//برچسب های پست
                Comments = _commentRepository.GetCommentByPostId(id)//نظرات پست
            };
            if (CommentStatus != null)
            {
                ViewBag.CommentStatus = CommentStatus;
            }

            return View(viewPost);
        }
        [Route("ShowNext/{id}")]
        public IActionResult NextPost(int id,bool? CommentStatus=null) //نمایش پست  
        {
            Post post =  _postRepository.GetNextPost(id); //پیدا کردن پست  و دریافت آن از Repository

            if (post == null)
            {
                return NotFound();
            }
            _postRepository.IncreaseVisitCount(id);

            ViewPostViewModel viewPost = new ViewPostViewModel()
            {
                Post = post,
                Categories = _postCategoryRepository.GetCategoriesFromPostCategoriesByPostId(id),//دسته بندی مربوطه
                PostTags = _postTagRepository.GetPostTagByPostId(id),//برچسب های پست
                Comments =  _commentRepository.GetCommentByPostId(id)//نظرات پست
            };
            if (CommentStatus!=null)
            {
                ViewBag.CommentStatus = CommentStatus;
            }
            
            return View(viewPost);
        }
        [Route("ShowPerv/{id}")]
        public IActionResult PreviosPost(int id, bool? CommentStatus = null) //نمایش پست  
        {
            Post post = _postRepository.GetPreiviosPost(id); //پیدا کردن پست  و دریافت آن از Repository

            if (post == null)
            {
                return NotFound();
            }
            _postRepository.IncreaseVisitCount(id);

            ViewPostViewModel viewPost = new ViewPostViewModel()
            {
                Post = post,
                Categories = _postCategoryRepository.GetCategoriesFromPostCategoriesByPostId(id),//دسته بندی مربوطه
                PostTags = _postTagRepository.GetPostTagByPostId(id),//برچسب های پست
                Comments = _commentRepository.GetCommentByPostId(id)//نظرات پست
            };
            if (CommentStatus != null)
            {
                ViewBag.CommentStatus = CommentStatus;
            }

            return View(viewPost);
        }
        [Route("p/{shortkey}")]
        public IActionResult ShortLink(string shortkey)//بازگرداندن به پست توسط لینک کوتاه
        {
            Post post = _postRepository.GetPostByShortKey(shortkey);//پیدا کردن پست از روی کلمه کوتاه
            if (post == null)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index), new {id = post.PostId, name = post.Title.ToUrl()});
        }


        [HttpPost]
        public IActionResult CreateComment([Bind("PostId,ParentId,Name,Email,Subject,CommentText")] Comment comment) //ایجاد نظر
        {
            if (ModelState.IsValid)
            {
                comment.CreateDate = DateTime.Now;
                comment.IsPublish = false;
                _commentRepository.InsertComment(comment);
                _commentRepository.Save();
                return Redirect(Request.Headers["Referer"].ToString()+"?CommentStatus=true");//بازگرداندن به صفحه درخواست کننده
            }
            else
            {
                return Redirect(Request.Headers["Referer"].ToString() + "?CommentStatus=false");
            }
        }
    }
}