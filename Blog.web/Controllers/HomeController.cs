﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Mvc;
using Blog.web.Models;
using Blog.ViewModels.Post;

namespace Blog.web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly IOptionRepository _optionRepository;
        private int itemPerPage;
        public HomeController(IPostRepository postRepository, IOptionRepository optionRepository)
        {
            _postRepository = postRepository;
            _optionRepository = optionRepository;
            _optionRepository = optionRepository;
            itemPerPage = _optionRepository.GetOption().ItemPerPage;
        }
        public async Task<IActionResult> Index(int pageId = 1)
        {
            //var postsPagination = await _postRepository.GetPostForIndexPagination("Football", pageId, itemPerPage);//دریافت اطلاعات نمایش از Repository مربوطه
            //postsPagination.option = _optionRepository.GetOption();
            var posts = await _postRepository.GetPostForIndexInfiniteScrollPagination("Football", pageId, itemPerPage);
            var vmodel = new PostInfiniteScrollViewModel
            {
                Posts = posts,
                TotalPages = await _postRepository.GetTotalPagePostsAsync("Football"),
                ItemPerPage = itemPerPage
            };

            return View(vmodel);
        }

        public async Task<JsonResult> GetPosts(int pageIndex)
        {
            try
            {
                var postAndOption = new PostAndOptionViewModel
                {
                    Posts = await _postRepository.GetPostForIndexInfiniteScrollPagination("Football", pageIndex, itemPerPage),
                    Option = _optionRepository.GetOption()
                };
                return Json(new { success = true, data = postAndOption });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, data = ex.Message });

            }

        }

        [Route("AboutUs")]
        public IActionResult About()
        {
            return View(_optionRepository.GetOption());
        }
        [Route("ContactUs")]
        public IActionResult Contact()
        {
            return View(_optionRepository.GetOption());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult ShowTeamVC(int League = 0)
        {
            return ViewComponent("LeaguesTeam", new { league = League });
        }
        public IActionResult ShowTeamVC2(int League = 0)
        {
            try
            {
                return ViewComponent("LeaguesTeam2", new { league = League });
            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }
        }
    }
}
