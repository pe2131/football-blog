﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.DomainClasses.commentForSlider;
using Blog.Services.IRepositories;
using Blog.ViewModels.Gallery;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.Controllers
{
    public class SliderController : Controller
    {
        public ISliderRepository _SliderRepository;
        private readonly ICommentRepositoryForSlider _commentRepositoryForSlider;

        public SliderController(ISliderRepository sliderRepository, ICommentRepositoryForSlider commentRepositoryForSlider)
        {
            _SliderRepository = sliderRepository;
            _commentRepositoryForSlider = commentRepositoryForSlider;
        }
        [Route("Gallery/{id?}/{name?}")]
        public IActionResult index(int id = 0, string name = "")
        {
            GalleryViewModel model = new GalleryViewModel();
            model.Other = _SliderRepository.GetSliderForShow().OrderByDescending(q => q.CreateDate).ToList();
            if (id == 0)
            {
                model.Current = model.Other.FirstOrDefault();
                _SliderRepository.IncreaseVisitCount(model.Current.SliderId);
            }
            else
            {
                model.Current = model.Other.Where(q => q.SliderId == id).FirstOrDefault();
                _SliderRepository.IncreaseVisitCount(model.Current.SliderId);
            }
            model.Comments = _commentRepositoryForSlider.GetCommentBySliderId(model.Current.SliderId);
            return View(model);
        }
        [Route("Galleryg/{id}/{name}")]
        public IActionResult RedirectToSliderUrl(int id, string name)
        {
            var slide = _SliderRepository.GetSliderById(id);
            if (slide == null)
            {
                return NotFound();
            }
            _SliderRepository.IncreaseVisitCount(id);
            return Redirect(slide.Url);
        }

        [HttpPost]
        public IActionResult CreateComment([Bind("SliderId,ParentId,Name,Email,Subject,CommentText")] CommentForSlider comment) //ایجاد نظر
        {
            if (ModelState.IsValid)
            {
                comment.CreateDate = DateTime.Now;
                comment.IsPublish = false;
                _commentRepositoryForSlider.InsertComment(comment);
                _commentRepositoryForSlider.Save();
                return Redirect(Request.Headers["Referer"].ToString() + "?CommentStatus=true");//بازگرداندن به صفحه درخواست کننده
            }
            else
            {
                return Redirect(Request.Headers["Referer"].ToString() + "?CommentStatus=false");
            }
        }
    }
}