﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.Controllers
{
    public class SearchController : Controller
    {
        private readonly IPostTagRepository _postTagRepository;
        private readonly IPostRepository _postRepository;
        private readonly IOptionRepository _optionRepository;
        private int postPerPage;
        public SearchController(IPostTagRepository postTagRepository, IPostRepository postRepository, IOptionRepository optionRepository)
        {
            _postTagRepository = postTagRepository;
            _postRepository = postRepository;
            _optionRepository = optionRepository;
            postPerPage = _optionRepository.GetOption().ItemPerPage;
        }

        [Route("tag/{tag}")]
        public IActionResult Tags(string tag, int pageId = 1)//جست و جو بر اساس برچست
        {
            return View("TagSearch", _postTagRepository.GetPostByTagPagination(tag/* برچسب مورد نظر*/, pageId, postPerPage));//گرفتن دیتا ی مورد نظر از لایه سرویس و ارسال به ویو
        }
        [Route("search")]
        public IActionResult Search(string search, int pageId = 1)//جست جو بر اساس کلمه
        {
            ViewData["search"] = search;
            return View("Search", _postRepository.GetPostByWordPagination(search, pageId, postPerPage));
        }
        [Route("category/{name}")]
        public IActionResult Category(string name, int pageId = 1)//جست و جو براساس دسته بندی
        {
            return View("CategorySearch", _postRepository.GetPostByCategoryPagination(name, pageId, postPerPage));
        }
        [Route("Author/{name}")]
        public IActionResult Author(string name, int pageId = 1)
        {
            return View("AuthorSearch", _postRepository.GetPostByAuthorPagination(name.Replace("-"," "), pageId, postPerPage));
        }
        public IActionResult OtherCats(string name, int pageId = 1)//جست و جو براساس دسته بندی
        {
            return View("CategorySearch", _postRepository.GetPostExeptCategoryPagination(name, pageId, postPerPage));
        }
    }
}