﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.web.Models.ManageViewModels
{
    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display]
        [Compare("Password")]

        public string ConfirmPassword { get; set; }

        public string StatusMessage { get; set; }
    }
}
