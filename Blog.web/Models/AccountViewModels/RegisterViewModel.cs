﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.web.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display]
        public string Name { get; set; }
        [EmailAddress]
        [Required]
        [Display]
        public string Email { get; set; }

        [Required]
        [StringLength(100,MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
