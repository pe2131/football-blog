﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.DataLayer.Context;
using Blog.DomainClasses;
using Blog.DomainClasses.Option;
using Microsoft.Extensions.DependencyInjection;

namespace Blog.web.Data
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<BlogContext>();
            context.Database.EnsureCreated();
            if (!context.Options.Any())
            {
                context.Options.Add(new Option()
                {
                    AboutUs = "",
                    ContactUs = "",
                    ItemPerPage = 3
                });

                context.Users.Add(new ApplicationUser()
                {
                    UserName = "admin@admin.com",
                    NormalizedUserName = "ADMIN@ADMIN.COM",
                    Email = "admin@admin.com",
                    NormalizedEmail = "ADMIN@ADMIN.COM",
                    PasswordHash =
                        "AQAAAAEAACcQAAAAEEthETaQMN4DxDwDkWIqoBuVXx9dLWsZB0BEus/6xJaGcTIffhuPrZhN1zx19VXNAQ==", //Admin@123456
                    SecurityStamp = "HT7APGLFVFDTCLMUL5BIV44QAB5TXXCA",
                    ConcurrencyStamp = "a371235e-4069-4b14-bd8b-f80d0a47a740",
                    LockoutEnabled = true,
                    IsActive = true,
                    Name = "Defult Admin"
                });

                context.SaveChanges();
            }
        }
    }
}
