﻿using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Blog.web.Services;

namespace Blog.web.Extensions
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link)
        {
            return emailSender.SendEmailAsync(email, "Confirmation",
                $"Please Enter the Link: <a href='{HtmlEncoder.Default.Encode(link)}'>لینک</a>");
        }
    }
}
