﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Slider;
using Blog.Services.IRepositories;
using Blog.Utilities.Images;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class SlidersController : Controller
    {
        private readonly ISliderRepository _sliderRepository;

        public SlidersController(ISliderRepository sliderRepository)
        {
            _sliderRepository = sliderRepository;
        }

        // GET: Admin/Sliders
        public IActionResult Index()
        {
            return View(_sliderRepository.GetAllSliders().Where(q => q.HasVideoe == false));
        }
        public IActionResult Videos()
        {
            return View(_sliderRepository.GetAllSliders().Where(q => q.HasVideoe == true));
        }
        // GET: Admin/Sliders/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slider = _sliderRepository.GetSliderById(id.Value);
            if (slider == null)
            {
                return NotFound();
            }

            return View(slider);
        }

        // GET: Admin/Sliders/Create
        public IActionResult Create()
        {
            return View();
        }
        public IActionResult CreateVideo()
        {
            return View();
        }
        // POST: Admin/Sliders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SliderId,Title,ImageName,Url,CreateDate,VisitCount,IsPublish")] Slider slider, List<IFormFile> imgFile)
        {
            if (ModelState.IsValid)
            {
                string images = "";
                if (imgFile.Count != 0)
                {
                    foreach (IFormFile img in imgFile)
                    {
                        string currentimg = Guid.NewGuid().ToString() + Path.GetExtension(img.FileName);
                        images += currentimg + ",";
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SliderImages", currentimg
                        );

                        Upload uploader = new Upload();
                        await Task.Run(() => uploader.UploadImage(savePath, img));
                    }
                    slider.ImageName = images.TrimEnd(',');
                    slider.CreateDate = DateTime.Now;
                    slider.IsPublish = false;
                    slider.VisitCount = 0;

                    _sliderRepository.InsertSlider(slider);
                    _sliderRepository.Save();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "you must select one image at least");
                    return View(slider);
                }

            }
            return View(slider);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateVideo([Bind("SliderId,Title,ImageName,Url,CreateDate,VisitCount,IsPublish,HasVideoe,VideoCode")] Slider slider, IFormFile imgFile)
        {
            if (ModelState.IsValid)
            {
                string images = "";
                if (imgFile != null)
                {
                    if (slider.VideoCode != null || !string.IsNullOrEmpty(slider.VideoCode))
                    {
                        ModelState.AddModelError(string.Empty, "you must select one Video or Video Code");
                        return View(slider);
                    }
                    string currentimg = Guid.NewGuid().ToString() + Path.GetExtension(imgFile.FileName);
                    images += currentimg + ",";
                    string savePath = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/SliderImages", currentimg
                    );

                    Upload uploader = new Upload();
                    await Task.Run(() => uploader.UploadImage(savePath, imgFile));
                    slider.VideoName = images.TrimEnd(',');
                    slider.CreateDate = DateTime.Now;
                    slider.IsPublish = false;
                    slider.VisitCount = 0;
                    _sliderRepository.InsertSlider(slider);
                    _sliderRepository.Save();
                    return RedirectToAction(nameof(Videos));
                }
                else
                {
                    if (slider.VideoCode != null || !string.IsNullOrEmpty(slider.VideoCode))
                    {
                        slider.VideoName = images.TrimEnd(',');
                        slider.CreateDate = DateTime.Now;
                        slider.IsPublish = false;
                        slider.VisitCount = 0;
                        _sliderRepository.InsertSlider(slider);
                        _sliderRepository.Save();
                        return RedirectToAction(nameof(Videos));
                    }
                    ModelState.AddModelError(string.Empty, "you must select one Video at least");
                    return View(slider);
                }

            }
            return View(slider);
        }
        // GET: Admin/Sliders/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slider = _sliderRepository.GetSliderById(id.Value);
            if (slider == null)
            {
                return NotFound();
            }
            return View(slider);
        }
        public IActionResult EditVideo(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slider = _sliderRepository.GetSliderById(id.Value);
            if (slider == null)
            {
                return NotFound();
            }
            return View(slider);
        }
        // POST: Admin/Sliders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SliderId,Title,ImageName,Url,CreateDate,VisitCount,IsPublish")] Slider slider, List<IFormFile> imgFile)
        {
            if (id != slider.SliderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string images = "";
                    foreach (IFormFile img in imgFile)
                    {
                        string currentimg = Guid.NewGuid().ToString() + Path.GetExtension(img.FileName);
                        images += currentimg + ",";
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SliderImages", currentimg
                        );

                        Upload uploader = new Upload();
                        await Task.Run(() => uploader.UploadImage(savePath, img));
                    }
                    var sliderinDb = _sliderRepository.GetSliderById(id);
                    if (sliderinDb.ImageName != "" && imgFile.Count > 0)
                    {
                        sliderinDb.ImageName += ",";
                    }
                    sliderinDb.ImageName += images.TrimEnd(',');
                    sliderinDb.IsPublish = slider.IsPublish;
                    sliderinDb.Title = slider.Title;
                    sliderinDb.Url = slider.Url;
                    _sliderRepository.UpdateSlider(sliderinDb);
                    _sliderRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SliderExists(slider.SliderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(slider);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditVideo(int id, [Bind("SliderId,Title,ImageName,Url,CreateDate,VisitCount,IsPublish,HasVideoe,VideoCode")] Slider slider, IFormFile imgFile)
        {
            if (id != slider.SliderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string images = "";
                    if (imgFile != null)
                    {

                        string currentimg = Guid.NewGuid().ToString() + Path.GetExtension(imgFile.FileName);
                        images += currentimg + ",";
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SliderImages", currentimg
                        );

                        Upload uploader = new Upload();
                        await Task.Run(() => uploader.UploadImage(savePath, imgFile));
                    }
                    var sliderinDb = _sliderRepository.GetSliderById(id);
                    sliderinDb.VideoName += images.TrimEnd(',');
                    sliderinDb.IsPublish = slider.IsPublish;
                    sliderinDb.Title = slider.Title;
                    sliderinDb.Url = slider.Url;
                    sliderinDb.VideoCode = slider.VideoCode;
                    _sliderRepository.UpdateSlider(sliderinDb);
                    _sliderRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SliderExists(slider.SliderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Videos));
            }
            return View(slider);
        }

        // GET: Admin/Sliders/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slider = _sliderRepository.GetSliderById(id.Value);
            if (slider == null)
            {
                return NotFound();
            }

            return View(slider);
        }

        // POST: Admin/Sliders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var slider = _sliderRepository.GetSliderById(id);
            bool isvideo = slider.HasVideoe;
            _sliderRepository.DeleteSlider(slider);
            if (!isvideo)
            {
                if (slider.ImageName != null)//حذف عکس مربوط به این اسلایدر 
                {
                    if (slider.ImageName.Contains(","))
                    {
                        string[] images = slider.ImageName.stringToArray();
                        foreach (var item in images)
                        {
                            var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/SliderImages", item);
                            Utilities.Images.Delete delete = new Delete();
                            delete.DeleteImage(imagePath);
                        }
                    }
                    else
                    {
                        var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/SliderImages", slider.ImageName);
                        Utilities.Images.Delete delete = new Delete();
                        delete.DeleteImage(imagePath);
                    }

                }
            }
            else
            {
                if (slider.VideoName.Contains(","))
                {
                    string[] videos = slider.VideoName.stringToArray();
                    foreach (var item in videos)
                    {
                        var videoPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/SliderImages", item);
                        Utilities.Images.Delete delete = new Delete();
                        delete.DeleteImage(videoPath);
                    }
                }
                else
                {
                    var videoPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/SliderImages", slider.VideoName);
                    Utilities.Images.Delete delete = new Delete();
                    delete.DeleteImage(videoPath);
                }
            }
            _sliderRepository.Save();
            if (isvideo)
            {
                return RedirectToAction(nameof(Videos));
            }
            return RedirectToAction(nameof(Index));
        }
        public IActionResult deleteImage(int id, string ImageName)
        {
            if (ImageName != null)
            {
                var slider = _sliderRepository.GetSliderById(id);
                if (slider.ImageName.Contains(","))
                {
                    slider.ImageName = slider.ImageName.DeleteSelectedString("," + ImageName);
                }
                else
                {
                    slider.ImageName = slider.ImageName.DeleteSelectedString(ImageName);
                }

                var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/SliderImages", ImageName);
                Utilities.Images.Delete delete = new Delete();
                delete.DeleteImage(imagePath);
                _sliderRepository.UpdateSlider(slider);
                _sliderRepository.Save();
                return RedirectToAction(nameof(Edit), new { id = id });
            }
            return RedirectToAction(nameof(Edit), new { id = id });

        }
        public IActionResult ChangeIsPublishStatus(int sliderId, bool isPublish, bool isVideo)//تغییر وضعیت انتشار اسلایدر
        {
            _sliderRepository.SliderPublishStatusChanger(sliderId, isPublish);
            if (isVideo)
            {
                return RedirectToAction(nameof(Videos));
            }
            return RedirectToAction(nameof(Index));
        }

        private bool SliderExists(int id)
        {
            return _sliderRepository.SliderExists(id);
        }
    }
}
