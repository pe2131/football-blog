﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public IActionResult Index()
        {
            return View(_userRepository.GetAllUsers());
        }

        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var slider = _userRepository.GetUserById(id);
            if (slider == null)
            {
                return NotFound();
            }

            return View(slider);
        }

        public IActionResult Delete(string id)
        {
            var slider = _userRepository.GetUserById(id);
            _userRepository.DeleteUser(slider);
            _userRepository.Save();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult ChangeIsActiveStatus(string userId, bool isActive)//تغییر وضعیت کاربر
        {
            _userRepository.UserActiveStatusChanger(userId, isActive);
            return RedirectToAction(nameof(Index));
        }

    }
}