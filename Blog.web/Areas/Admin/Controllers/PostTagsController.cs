﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.PostTag;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class PostTagsController : Controller
    {
        private readonly IPostTagRepository _postTagRepository;
        private readonly IPostRepository _postRepository;

        public PostTagsController(IPostTagRepository postTagRepository, IPostRepository postRepository)
        {
            _postTagRepository = postTagRepository;
            _postRepository = postRepository;
        }
        //برای آشنایی بیشتر با مسیر یابی در 
        //Asp.net Core 
        //به آدرس زیر مراجعه کنید
        //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-2.1

        // GET: Admin/PostTags
        [Route("admin/postTags/{postId:int:max(0)?}")]
        public IActionResult Index()
        {
            ViewData["postId"] = 0;
            return View(_postTagRepository.GetAllPostTags());
        }

        [Route("admin/postTags/{postId:int:min(1)}")]
        public IActionResult Index(int postId)
        {
            ViewData["postId"] = postId;
            return View(_postTagRepository.GetPostTagByPostId(postId));

        }


        // GET: Admin/PostTags/Details/5
        public IActionResult Details(int? id, int postId = 0)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postTag = _postTagRepository.GetPostTagById(id.Value);
            if (postTag == null)
            {
                return NotFound();
            }
            ViewData["postId"] = postId;
            return View(postTag);
        }

        // GET: Admin/PostTags/Create
        [HttpGet]
        [Route("admin/PostTags/Create/{postId:int:min(1)}")]
        public IActionResult Create(int postId)
        {
            //ViewData["PostId"] = new SelectList(_postRepository.GetPostsById(postId), "PostId", "Title");
            ViewData["postId"] = postId;
            return View();
        }
        // GET: Admin/PostTags/Create
        [HttpGet]
        [Route("admin/PostTags/Create/{postId:int:max(0)}")]
        public IActionResult Create()
        {
            ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            ViewData["postId"] = 0;
            return View();
        }

        // POST: Admin/PostTags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("admin/PostTags/Create/{postId:int}")]
        public IActionResult Create([Bind("TagId,PostId,Tag")] PostTag postTag, int mPostId = 0)
        {
            if (ModelState.IsValid)
            {
                if (postTag.Tag.Contains(","))
                {
                    foreach (var item in postTag.Tag.Split(",", StringSplitOptions.RemoveEmptyEntries))
                    {
                        
                        _postTagRepository.InsertPostTag(new PostTag()
                        {
                            PostId = postTag.PostId,
                            Tag = item
                        });
                    }
                }
                else
                {
                    _postTagRepository.InsertPostTag(postTag);
                }
                
                _postTagRepository.Save();
                return RedirectToAction(nameof(Index), new { postId = mPostId });
            }
            if (mPostId == 0)
            {
                ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            }
            ViewData["postId"] = mPostId;
            return View(postTag);
        }

        // GET: Admin/PostTags/Edit/5
        [HttpGet]
        [Route("admin/PostTags/Edit/{id}/{postId:int:min(1)}")]
        public IActionResult Edit(int? id, int postId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postTag = _postTagRepository.GetPostTagById(id.Value);
            if (postTag == null)
            {
                return NotFound();
            }
            //ViewData["PostId"] = new SelectList(_postRepository.GetPostsById(postId), "PostId", "Title");
            ViewData["postId"] = postId;
            return View(postTag);
        }
        [HttpGet]
        [Route("admin/PostTags/Edit/{id}/{postId:int:max(0)}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postTag = _postTagRepository.GetPostTagById(id.Value);
            if (postTag == null)
            {
                return NotFound();
            }
            ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title", postTag.PostId);
            ViewData["postId"] = 0;
            return View(postTag);
        }

        // POST: Admin/PostTags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("admin/PostTags/Edit/{id}/{postId:int}")]
        public IActionResult Edit(int id, [Bind("TagId,PostId,Tag")] PostTag postTag, int mPostId = 0)
        {
            if (id != postTag.TagId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _postTagRepository.UpdatePostTag(postTag);
                    _postTagRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostTagExists(postTag.TagId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { postId = mPostId });
            }
            if (mPostId == 0)
            {
                ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            }
            ViewData["postId"] = mPostId;
            return View(postTag);
        }

        // GET: Admin/PostTags/Delete/5
        public IActionResult Delete(int? id, int postId = 0)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postTag = _postTagRepository.GetPostTagById(id.Value);
            if (postTag == null)
            {
                return NotFound();
            }
            ViewData["postId"] = postId;
            return View(postTag);
        }

        // POST: Admin/PostTags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id, int postId = 0)
        {
            try
            {
                var postTag = _postTagRepository.GetPostTagById(id);
                _postTagRepository.DeletePostTag(postTag);
                _postTagRepository.Save();
                if (postId != 0)
                {
                    return RedirectToAction(nameof(Index), new { postId = postId });
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }

        }

        private bool PostTagExists(int id)
        {
            return _postTagRepository.PostTagExists(id);
        }
    }
}
