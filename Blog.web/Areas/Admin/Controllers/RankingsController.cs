﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Ranking;
using Microsoft.AspNetCore.Authorization;
using Blog.ViewModels.Rankings;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class RankingsController : Controller
    {
        private readonly BlogContext _context;

        public RankingsController(BlogContext context)
        {
            _context = context;
        }

        // GET: Admin/Rankings

        public async Task<IActionResult> Index(int? LeaugeId)
        {
            var leagues = _context.leagues.ToList();

            ViewData["Leagues"] = new SelectList(leagues, "Id", "Name");
            LeaugeId = LeaugeId == null ? leagues.FirstOrDefault().Id : LeaugeId;

            var vModel = new ShowRankingsViewModel
            {
                Rankings = await _context.Rankings.Include(r => r.Teames).Where(d => d.Teames.LeagueId == LeaugeId).ToListAsync(),
                LeaugeId = (int)LeaugeId
            };

            return View(vModel);
        }

        // GET: Admin/Rankings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ranking = await _context.Rankings
                .Include(r => r.Teames)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ranking == null)
            {
                return NotFound();
            }

            return View(ranking);
        }

        // GET: Admin/Rankings/Create
        public IActionResult Create()
        {
            ViewData["TeamId"] = new SelectList(_context.Teams, "id", "Name");
            return View();
        }

        // POST: Admin/Rankings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,TeamId,Rank,GF,GD")] Ranking ranking)
        {
            if (ModelState.IsValid)
            {
                var rankinginDb = _context.Rankings.Where(q => q.TeamId == ranking.TeamId).FirstOrDefault();
                if (rankinginDb != null)
                {
                    ModelState.AddModelError(string.Empty, "This tim is exist in rank table please Edit it");
                    return View(ranking);
                }
                _context.Add(ranking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TeamId"] = new SelectList(_context.Teams, "id", "Name", ranking.TeamId);
            return View(ranking);
        }

        // GET: Admin/Rankings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ranking = await _context.Rankings.FindAsync(id);
            if (ranking == null)
            {
                return NotFound();
            }
            ViewData["TeamId"] = new SelectList(_context.Teams, "id", "Name", ranking.TeamId);
            return View(ranking);
        }

        // POST: Admin/Rankings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,TeamId,Rank,GF,GD")] Ranking ranking)
        {
            if (id != ranking.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ranking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RankingExists(ranking.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TeamId"] = new SelectList(_context.Teams, "id", "Name", ranking.TeamId);
            return View(ranking);
        }

        // GET: Admin/Rankings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ranking = await _context.Rankings
                .Include(r => r.Teames)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ranking == null)
            {
                return NotFound();
            }

            return View(ranking);
        }

        // POST: Admin/Rankings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var ranking = await _context.Rankings.FindAsync(id);
                _context.Rankings.Remove(ranking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }

        }

        private bool RankingExists(int id)
        {
            return _context.Rankings.Any(e => e.Id == id);
        }
    }
}
