﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Comment;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class CommentsController : Controller
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IPostRepository _postRepository;
        private readonly IUserRepository _userRepository;

        public CommentsController(ICommentRepository commentRepository, IPostRepository postRepository, IUserRepository userRepository)
        {
            _commentRepository = commentRepository;
            _postRepository = postRepository;
            _userRepository = userRepository;
        }

        // GET: Admin/Comments
        public  IActionResult Index()
        {
            //return View(_commentRepository.GetAllParentComments());//بدست آوردت تمامی نظر های اصلی
            return View(_commentRepository.GetAllComments().OrderByDescending(o=>o.CreateDate));
        }

        // GET: Admin/Comments/Details/5
        public  IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRepository.GetCommentById(id.Value);//بدست آوردن نظر توسط شناسه مشخص
            if (comment == null)
            {
                return NotFound();
            }

            ViewData["subComments"] = _commentRepository.GetAllCommentsForParent(id.Value);//بدست آوردن تمامی نظرات زیر مجموعه نظر بدست آمده
            return View(comment);
        }

        // GET: Admin/Comments/Create
        public  IActionResult Create(int parentId = 0, int postId = 0)//ParentId زمانی دارای مقدار است که در حال ارسال پاسخ نطر هستید
        {
            ViewData["ParentId"] = (parentId != 0) ? parentId : (object)null;
            if (postId != 0)
            {
                ViewData["PostId"] = postId;
            }
            else
            {
                ViewData["PostId"] = 0;
                ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            }

            return View();
        }

        // POST: Admin/Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  IActionResult Create([Bind("CommentId,PostId,ParentId,Name,CommentText,CreateDate,IsPublish")] Comment comment,int postId)
        {
            comment.Name = _userRepository.GetUserByName(User.Identity.Name).Name;//بدست آوردن شناسه نویسنده
            if (ModelState.IsValid)
            {
                comment.IsPublish = false;
                comment.CreateDate = DateTime.Now;
                
                _commentRepository.InsertComment(comment);
                _commentRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentId"] = new SelectList(_commentRepository.GetAllComments(), "CommentId", "CommentText", comment.ParentId);

            if (postId != 0)
            {
                ViewData["PostId"] = postId;
            }
            else
            {
                ViewData["PostId"] = 0;
                ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            }

            return View(comment);
        }

        // GET: Admin/Comments/Edit/5
        public  IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRepository.GetCommentById(id.Value);//بدست آوردن یک نظر 
            if (comment == null)
            {
                return NotFound();
            }
            ViewData["PostId"] = comment.PostId;
            ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Description", comment.PostId);
            return View(comment);
        }

        // POST: Admin/Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  IActionResult Edit(int id, [Bind("CommentId,PostId,ParentId,Name,CommentText,CreateDate,IsPublish")] Comment comment)
        {
            if (id != comment.CommentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _commentRepository.UpdateComment(comment);
                    _commentRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommentExists(comment.CommentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentId"] = new SelectList(_commentRepository.GetAllComments(), "CommentId", "CommentText", comment.ParentId);
            ViewData["PostId"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Description", comment.PostId);
            return View(comment);
        }

        // GET: Admin/Comments/Delete/5
        public  IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRepository.GetCommentById(id.Value);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: Admin/Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public  IActionResult DeleteConfirmed(int id)
        {
            var comment = _commentRepository.GetCommentById(id);//بدست آوردن یک نظر
            _commentRepository.DeleteComment(comment);//حذف یک نظر
            _commentRepository.Save();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult ChangeIsPublishStatus(int commentId, bool isPublish)//تغییر وضعیت نظر
        {
            _commentRepository.CommentPublishStatusChanger(commentId, isPublish);
            return RedirectToAction(nameof(Index));
        }

        private bool CommentExists(int id)//کنترل وجود یا عدم وجود یک نظر
        {
            return _commentRepository.CommentExists(id);
        }
    }
}
