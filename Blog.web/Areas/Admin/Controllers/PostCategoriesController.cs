﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DomainClasses.PostCategory;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Authorization;
using System;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class PostCategoriesController : Controller
    {
        private readonly IPostCategoryRepository _postCategoryRepository;
        private readonly IPostRepository _postRepository;
        private readonly ICategoryRepository _categoryRepository;
        public PostCategoriesController(IPostCategoryRepository postCategoryRepository, IPostRepository postRepository, ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
            _postRepository = postRepository;
            _postCategoryRepository = postCategoryRepository;
        }

        //برای آشنایی بیشتر با مسیر یابی در 
        //Asp.net Core 
        //به آدرس زیر مراجعه کنید
        //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-2.1

        [Route("admin/postcategories/{postId:int:max(0)?}")]//زمانی که فیلید postId بدون مقدار و یا دارای مقدار حداکثر 0 باشد
        public IActionResult Index()
        {
            var list = _postCategoryRepository.GetAllPostCategories();
            ViewData["postId"] = 0;
            return View(list);
        }
        // GET: Admin/PostCategories
        [Route("admin/postcategories/{postId:int:min(1)}")]//زمانی که فیلید postId دارای حداقل مقدار 1 باشد
        public IActionResult Index(int postId)
        {
            ViewData["postId"] = postId;
            return View(_postCategoryRepository.GetPostCategoriesByPostId(postId));
        }

        // GET: Admin/PostCategories/Details/5
        public IActionResult Details(int? id, int postId = 0)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postCategory = _postCategoryRepository.GetPostCategoryById(id.Value);
            if (postCategory == null)
            {
                return NotFound();
            }
            ViewData["postId"] = postId;
            return View(postCategory);
        }

        // GET: Admin/PostCategories/Create
        [HttpGet]
        [Route("admin/PostCategories/Create/{postId:int:min(1)}")]//زمانی که فیلید postId دارای حداقل مقدار 1 باشد
        public IActionResult Create(int postId)
        {
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");
            ViewData["Posts"] = new SelectList(_postRepository.GetPostsById(postId), "PostId", "Title");
            ViewData["postId"] = postId;
            return View();
        }
        [HttpGet]
        [Route("admin/postcategories/Create/{postId:int:max(0)}")]//زمانی که فیلید postId بدون مقدار و یا دارای مقدار حداکثر 0 باشد
        public IActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");
            ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            ViewData["postId"] = 0;
            return View();
        }

        // POST: Admin/PostCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("admin/postcategories/Create/{postId:int}")]
        public IActionResult Create([Bind("PostCategoryId,PostId,CategoryId")] PostCategory postCategory, int mPostId = 0)
        {
            if (ModelState.IsValid)
            {
                _postCategoryRepository.InsertPostCategory(postCategory);
                _postCategoryRepository.Save();
                return RedirectToAction(nameof(Index), new { postId = mPostId });
            }
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");
            if (mPostId == 0)
            {
                ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            }
            else
            {
                ViewData["Posts"] = new SelectList(_postRepository.GetPostsById(mPostId), "PostId", "Title");
            }
            ViewData["postId"] = mPostId;
            return View(postCategory);
        }

        // GET: Admin/PostCategories/Edit/5
        [Route("admin/postcategories/Edit/{id}/{postId:int:min(1)}")]//زمانی که فیلید idدارای حداقل مقدار 1 باشد
        [HttpGet]
        public IActionResult Edit(int? id, int postId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postCategory = _postCategoryRepository.GetPostCategoryById(id.Value);
            if (postCategory == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");

            ViewData["Posts"] = new SelectList(_postRepository.GetPostsById(postId), "PostId", "Title");

            ViewData["postId"] = postId;
            return View(postCategory);
        }
        [Route("admin/postcategories/Edit/{id}/{postId:int:max(0)}")]//زمانی که فیلید idبدون مقدار و یا دارای مقدار حداکثر 0 باشد
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postCategory = _postCategoryRepository.GetPostCategoryById(id.Value);
            if (postCategory == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");

            ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");

            ViewData["postId"] = 0;
            return View(postCategory);
        }

        // POST: Admin/PostCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("admin/postcategories/Edit/{id}/{postId:int}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("PostCategoryId,PostId,CategoryId")] PostCategory postCategory, int mPostId = 0)
        {
            if (id != postCategory.PostCategoryId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _postCategoryRepository.UpdatePostCategory(postCategory);//آپدیت رابطه بین پست و دسته بندی 
                    _postCategoryRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostCategoryExists(postCategory.PostCategoryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { postId = mPostId });
            }
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");//بدست آوردن دسته بندی ها
            if (mPostId == 0)
            {//زمانی که به صورت مستقیم وارد قسمت مدیریت ارتباط با دسته بندی میشیم
                ViewData["Posts"] = new SelectList(_postRepository.GetAllPosts(), "PostId", "Title");
            }
            else
            {//زمانی که از طریق مدیریت پست وارد قسمت مدیریت ارتباط با دسته بندی میشیم
                ViewData["Posts"] = new SelectList(_postRepository.GetPostsById(mPostId), "PostId", "Title");
            }
            ViewData["postId"] = mPostId;
            return View(postCategory);
        }

        // GET: Admin/PostCategories/Delete/5
        public IActionResult Delete(int? id, int postId = 0)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postCategory = _postCategoryRepository.GetPostCategoryById(id.Value);//بدست آوردن یک رابط دسته بندی و پست
            if (postCategory == null)
            {
                return NotFound();
            }
            ViewData["postId"] = postId;
            return View(postCategory);
        }

        // POST: Admin/PostCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id, int postId = 0)
        {
            try
            {
                var postCategory = _postCategoryRepository.GetPostCategoryById(id);//بدست آورن یک رابطه دسته بندی با پست
                _postCategoryRepository.DeletePostCategory(postCategory);//حذف یک رابطه دسته بندی و پست
                _postCategoryRepository.Save();
                if (postId != 0)
                {
                    return RedirectToAction(nameof(Index), new { postId });
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }
        }

        private bool PostCategoryExists(int id)//کنترل وجود یا عدم وجود
        {
            return _postCategoryRepository.PostCategoryExists(id);
        }
    }
}
