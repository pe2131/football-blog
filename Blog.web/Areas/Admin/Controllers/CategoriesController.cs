﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Category;
using Blog.Services.IRepositories;
using Blog.Services.Services;
using Microsoft.AspNetCore.Authorization;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class CategoriesController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        // GET: Admin/Categories
        public IActionResult Index()
        {
            return View(_categoryRepository.GetAllCategories());
        }

        // GET: Admin/Categories/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category =  _categoryRepository.GetCategoryById(id.Value);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // GET: Admin/Categories/Create
        public IActionResult Create(int Id = 0)
        {
            ViewData["ParentId"] = Id;

            return View();
        }

        // POST: Admin/Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("CategoryId,Name,NameInUrl,ParentId")] Category category)
        {
            if (ModelState.IsValid)
            {
                if (category.ParentId==0)
                {
                    category.ParentId = null;
                }
                 _categoryRepository.InsertCategory(category);
                 _categoryRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentId"] = new SelectList( _categoryRepository.GetAllCategories(), "CategoryId", "Name", category.ParentId);
            return View(category);
        }

        // GET: Admin/Categories/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category =  _categoryRepository.GetCategoryById(id.Value);//بدست آوردن دسته بندی با شناسه مشخص
            if (category == null)
            {
                return NotFound();
            }
            ViewData["ParentId"] = new SelectList( _categoryRepository.GetAllCategories(), "CategoryId", "Name", category.ParentId);//دریافت تمام دسته بندی ها
            return View(category);
        }

        // POST: Admin/Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("CategoryId,Name,NameInUrl,ParentId")] Category category)
        {
            if (id != category.CategoryId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (category.ParentId == 0)
                    {
                        category.ParentId = null;
                    }
                     _categoryRepository.UpdateCategory(category);//به روز رسانی در دسته بندی در دیتا بیس
                     _categoryRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! CategoryExists(category.CategoryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentId"] = new SelectList( _categoryRepository.GetAllCategories(), "CategoryId", "Name", category.ParentId);//دریافت تمام دسته بندی ها
            return View(category);
        }

        // GET: Admin/Categories/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category =  _categoryRepository.GetCategoryById(id.Value);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: Admin/Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                var category = _categoryRepository.GetCategoryById(id);
                _categoryRepository.DeleteCategory(category);
                _categoryRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }

        }

        private bool CategoryExists(int id)//چک کردن وجود داشتن یه دسته بندی
        {
            return _categoryRepository.CategoryExists(id);
        }
    }
}
