﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Option;
using Blog.DomainClasses.Slider;
using Blog.Services.IRepositories;
using Blog.Utilities.Images;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class OptionsController : Controller
    {
        private readonly IOptionRepository _optionRepository;

        public OptionsController(IOptionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }
        [Route("Admin/Options")]
        public IActionResult Edit()
        {
            var option = _optionRepository.GetOption();//دریافت تنظیمات بلاگ 
            if (option == null)
            {
                return NotFound();
            }
            return View(option);
        }

        // POST: Admin/Sliders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Admin/Options")]
        public IActionResult Edit(int id, Option option, IFormFile BannerImage1, IFormFile BannerImage2, IFormFile BannerImage3, IFormFile BannerImage4, IFormFile BannerImage5,IFormFile Logo)
        {
            if (id != option.Id)
            {
                return NotFound();
            }
            try
            {
                if (ModelState.IsValid)
                {
                    if (BannerImage1 != null && (option.TopBannerCode != null || !string.IsNullOrEmpty(option.TopBannerCode)))
                    {
                        ModelState.AddModelError("Error", "it cant both of banner image1 and TopBannerCode was filled!! ");
                        return View(option);
                    }
                    if (BannerImage2 != null && (option.SideBannerCode1 != null || !string.IsNullOrEmpty(option.SideBannerCode1)))
                    {
                        ModelState.AddModelError("Error", "it cant both of banner image2 and SideBannerCode1 was filled!! ");
                        return View(option);
                    }
                    if (BannerImage3 != null && (option.SideBannerCode2 != null || !string.IsNullOrEmpty(option.SideBannerCode2)))
                    {
                        ModelState.AddModelError("Error", "it cant both of banner image3 and SideBannerCode2 was filled!! ");
                        return View(option);
                    }
                    if (BannerImage4 != null && (option.BetWeenBannerCode != null || !string.IsNullOrEmpty(option.BetWeenBannerCode)))
                    {
                        ModelState.AddModelError("Error", "it cant both of banner image4 and BetWeenBannerCode was filled!! ");
                        return View(option);
                    }
                    if (BannerImage5 != null && (option.FooterBannerCode != null || !string.IsNullOrEmpty(option.FooterBannerCode)))
                    {
                        ModelState.AddModelError("Error", "it cant both of banner image5 and FooterBannerCode was filled!! ");
                        return View(option);
                    }
                    if (BannerImage1 != null)
                    {
                        option.TopBannerImage = Guid.NewGuid().ToString() + Path.GetExtension(BannerImage1.FileName);
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SettingImages", option.TopBannerImage
                        );

                        Upload uploader = new Upload();
                        uploader.UploadImage(savePath, BannerImage1);
                    }
                    if (BannerImage2 != null)
                    {
                        option.SideBannerImage1 = Guid.NewGuid().ToString() + Path.GetExtension(BannerImage2.FileName);
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SettingImages", option.SideBannerImage1
                        );

                        Upload uploader = new Upload();
                        uploader.UploadImage(savePath, BannerImage2);
                    }
                    if (BannerImage3 != null)
                    {
                        option.SideBannerImage2 = Guid.NewGuid().ToString() + Path.GetExtension(BannerImage3.FileName);
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SettingImages", option.SideBannerImage2
                        );

                        Upload uploader = new Upload();
                        uploader.UploadImage(savePath, BannerImage3);
                    }
                    if (BannerImage4 != null)
                    {
                        option.BetWeenBannerImage = Guid.NewGuid().ToString() + Path.GetExtension(BannerImage4.FileName);
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SettingImages", option.BetWeenBannerImage
                        );

                        Upload uploader = new Upload();
                        uploader.UploadImage(savePath, BannerImage4);
                    }
                    if (BannerImage5 != null)
                    {
                        option.FooterBannerImage = Guid.NewGuid().ToString() + Path.GetExtension(BannerImage5.FileName);
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SettingImages", option.FooterBannerImage
                        );

                        Upload uploader = new Upload();
                        uploader.UploadImage(savePath, BannerImage5);
                    }
                    if (Logo != null)
                    {
                        option.Logo = Guid.NewGuid().ToString() + Path.GetExtension(Logo.FileName);
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/SettingImages", option.Logo
                        );

                        Upload uploader = new Upload();
                        uploader.UploadImage(savePath, Logo);
                    }
                    var optionInDb = _optionRepository.GetOption();
                    optionInDb.TopBannerImage = option.TopBannerImage == null ? optionInDb.TopBannerImage : option.TopBannerImage;
                    optionInDb.SideBannerImage1 = option.SideBannerImage1 == null ? optionInDb.SideBannerImage1 : option.SideBannerImage1;
                    optionInDb.SideBannerImage2 = option.SideBannerImage2 == null ? optionInDb.SideBannerImage2 : option.SideBannerImage2;
                    optionInDb.BetWeenBannerImage = option.BetWeenBannerImage == null ? optionInDb.BetWeenBannerImage : option.BetWeenBannerImage;
                    optionInDb.FooterBannerImage = option.FooterBannerImage == null ? optionInDb.FooterBannerImage : option.FooterBannerImage;
                    optionInDb.TopBannerCode = option.TopBannerCode;
                    optionInDb.SideBannerCode1 = option.SideBannerCode1;
                    optionInDb.SideBannerCode2 = option.SideBannerCode2;
                    optionInDb.BetWeenBannerCode = option.BetWeenBannerCode;
                    optionInDb.FooterBannerCode = option.FooterBannerCode;
                    optionInDb.ContactUs = option.ContactUs;
                    optionInDb.AboutUs = option.AboutUs;
                    optionInDb.Logo = option.Logo == null ? optionInDb.Logo : option.Logo;
                    optionInDb.Footer = option.Footer;
                    optionInDb.ItemPerPage = option.ItemPerPage;

                    _optionRepository.UpdateOption(optionInDb);//آپدیت تنظیمات 
                    _optionRepository.Save();//ذخیره تنظیمات
                }
                return View(option);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

    }
}
