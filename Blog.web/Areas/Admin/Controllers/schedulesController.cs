﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.schedules;
using Microsoft.AspNetCore.Authorization;
using Blog.ViewModels.Schedules;


namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class schedulesController : Controller
    {
        private readonly BlogContext _context;

        public schedulesController(BlogContext context)
        {
            _context = context;
        }

        // GET: Admin/schedules
        public async Task<IActionResult> Index(int? LeaugeId)
        {
            var leagues = _context.leagues.ToList();

            ViewData["Leagues"] = new SelectList(leagues, "Id", "Name");
            LeaugeId = LeaugeId == null ? leagues.FirstOrDefault().Id : LeaugeId;

            var vModel = new ShowSchedulesViewModel
            {
                Schedules = await _context.schedules.Include(s => s.AwayTeams).Include(s => s.HomeTeams).Where(d => d.AwayTeams.LeagueId == LeaugeId).ToListAsync(),
                LeaugeId = (int)LeaugeId
            };

            return View(vModel);
        }
        // GET: Admin/schedules/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schedules = await _context.schedules
                .Include(s => s.AwayTeams)
                .Include(s => s.HomeTeams)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schedules == null)
            {
                return NotFound();
            }

            return View(schedules);
        }

        // GET: Admin/schedules/Create
        public IActionResult Create()
        {
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "id", "Name");
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "id", "Name");
            return View();
        }

        // POST: Admin/schedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,HomeTeamId,AwayTeamId,MatchDay")] schedules schedules)
        {
            if (ModelState.IsValid)
            {
                if (schedules.AwayTeamId == schedules.HomeTeamId)
                {
                    ModelState.AddModelError(string.Empty, "Both Team can not be same!!");
                    ViewData["AwayTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.AwayTeamId);
                    ViewData["HomeTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.HomeTeamId);
                    return View(schedules);
                }
                _context.Add(schedules);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.AwayTeamId);
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.HomeTeamId);
            return View(schedules);
        }

        // GET: Admin/schedules/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schedules = await _context.schedules.FindAsync(id);
            if (schedules == null)
            {
                return NotFound();
            }
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.AwayTeamId);
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.HomeTeamId);
            return View(schedules);
        }

        // POST: Admin/schedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,HomeTeamId,AwayTeamId,MatchDay")] schedules schedules)
        {
            if (id != schedules.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (schedules.AwayTeamId == schedules.HomeTeamId)
                    {
                        ModelState.AddModelError(string.Empty, "Both Team can not be same!!");
                        ViewData["AwayTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.AwayTeamId);
                        ViewData["HomeTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.HomeTeamId);
                        return View(schedules);
                    }
                    var schedulesinDb = _context.schedules.Find(id);
                    schedulesinDb.AwayTeamId = schedules.AwayTeamId;
                    schedulesinDb.HomeTeamId = schedules.HomeTeamId;
                    schedulesinDb.MatchDay = schedules.MatchDay;
                    _context.Update(schedulesinDb);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!schedulesExists(schedules.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.AwayTeamId);
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "id", "Name", schedules.HomeTeamId);
            return View(schedules);
        }

        // GET: Admin/schedules/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schedules = await _context.schedules
                .Include(s => s.AwayTeams)
                .Include(s => s.HomeTeams)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schedules == null)
            {
                return NotFound();
            }

            return View(schedules);
        }

        // POST: Admin/schedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var schedules = await _context.schedules.FindAsync(id);
                _context.schedules.Remove(schedules);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }
        }

        private bool schedulesExists(int id)
        {
            return _context.schedules.Any(e => e.Id == id);
        }
    }
}
