﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Results;
using Microsoft.AspNetCore.Authorization;
using Blog.ViewModels.Result;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class ResultsController : Controller
    {
        private readonly BlogContext _context;

        public ResultsController(BlogContext context)
        {
            _context = context;
        }

        // GET: Admin/Results
        public async Task<IActionResult> Index(int? LeaugeId)
        {
            var leagues = _context.leagues.ToList();

            ViewData["Leagues"] = new SelectList(leagues, "Id", "Name");
            LeaugeId = LeaugeId == null ? leagues.FirstOrDefault().Id : LeaugeId;

            var vModel = new ShowResultsViewModel
            {
                Results = await _context.Results.Include(r => r.AwayTeams).Include(r => r.HomeTeams).Where(d => d.AwayTeams.LeagueId == LeaugeId || d.HomeTeams.LeagueId == LeaugeId).ToListAsync(),
                LeaugeId = (int)LeaugeId
            };

            return View(vModel);
        }

        // GET: Admin/Results/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await _context.Results
                .Include(r => r.AwayTeams)
                .Include(r => r.HomeTeams)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (results == null)
            {
                return NotFound();
            }

            return View(results);
        }

        // GET: Admin/Results/Create
        public IActionResult Create()
        {
            ViewData["Team2Id"] = new SelectList(_context.Teams, "id", "Name");
            ViewData["Team1Id"] = new SelectList(_context.Teams, "id", "Name");
            return View();
        }

        // POST: Admin/Results/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Team1Id,Team1Goals,Team2Id,Team2Goals,CreatedAt")] Results results)
        {
            results.CreatedAt = DateTime.Now;
            if (ModelState.IsValid)
            {
                if (results.Team1Id == results.Team2Id)
                {
                    ModelState.AddModelError(string.Empty, "both team can not be same!!");
                    ViewData["Team2Id"] = new SelectList(_context.Teams, "id", "Name", results.Team2Id);
                    ViewData["Team1Id"] = new SelectList(_context.Teams, "id", "Name", results.Team1Id);
                    return View(results);
                }
                _context.Add(results);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Team2Id"] = new SelectList(_context.Teams, "id", "Name", results.Team2Id);
            ViewData["Team1Id"] = new SelectList(_context.Teams, "id", "Name", results.Team1Id);
            return View(results);
        }

        // GET: Admin/Results/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await _context.Results.FindAsync(id);
            if (results == null)
            {
                return NotFound();
            }
            ViewData["Team2Id"] = new SelectList(_context.Teams, "id", "Name", results.Team2Id);
            ViewData["Team1Id"] = new SelectList(_context.Teams, "id", "Name", results.Team1Id);
            return View(results);
        }

        // POST: Admin/Results/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Team1Id,Team1Goals,Team2Id,Team2Goals,CreatedAt")] Results results)
        {
            if (id != results.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (results.Team1Id == results.Team2Id)
                    {
                        ModelState.AddModelError(string.Empty, "both team can not be same!!");
                        ViewData["Team2Id"] = new SelectList(_context.Teams, "id", "Name", results.Team2Id);
                        ViewData["Team1Id"] = new SelectList(_context.Teams, "id", "Name", results.Team1Id);
                        return View(results);
                    }
                    var resultInDb = _context.Results.Find(id);
                    resultInDb.CreatedAt = DateTime.Now;
                    resultInDb.Team1Goals = results.Team1Goals;
                    resultInDb.Team2Goals = results.Team2Goals;
                    resultInDb.Team1Id = results.Team1Id;
                    resultInDb.Team2Id = results.Team2Id;
                    _context.Update(resultInDb);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ResultsExists(results.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Team2Id"] = new SelectList(_context.Teams, "id", "Name", results.Team2Id);
            ViewData["Team1Id"] = new SelectList(_context.Teams, "id", "Name", results.Team1Id);
            return View(results);
        }

        // GET: Admin/Results/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await _context.Results
                .Include(r => r.AwayTeams)
                .Include(r => r.HomeTeams)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (results == null)
            {
                return NotFound();
            }

            return View(results);
        }

        // POST: Admin/Results/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var results = await _context.Results.FindAsync(id);
                _context.Results.Remove(results);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }

        }

        private bool ResultsExists(int id)
        {
            return _context.Results.Any(e => e.Id == id);
        }
    }
}
