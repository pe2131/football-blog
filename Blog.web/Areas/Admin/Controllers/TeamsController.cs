﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Teames;
using Blog.Services.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Blog.Utilities.Images;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class TeamsController : Controller
    {
        private readonly ITeamRepository _teamRepository;
        private readonly BlogContext _context;

        public TeamsController(ITeamRepository teamRepository, BlogContext context)
        {
            _teamRepository = teamRepository;
            _context = context;
        }

        // GET: Admin/Teams
        public IActionResult Index()
        {
            return View(_teamRepository.GetTeamsByLeague());
        }

        // GET: Admin/Teams/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teams = _teamRepository.GetTeamById(id.Value);
            if (teams == null)
            {
                return NotFound();
            }

            return View(teams);
        }

        // GET: Admin/Teams/Create
        public IActionResult Create()
        {
            ViewData["Leagues"]= new SelectList(_context.leagues,"Id","Name");
            return View();
        }

        // POST: Admin/Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("id,Name,Logo,LeagueId")] Teams teams, IFormFile imgFile)
        {
            if (ModelState.IsValid)
            {
                ViewData["Leagues"] = new SelectList(_context.leagues, "Id", "Name");
                if (imgFile == null)
                {
                    ModelState.AddModelError(string.Empty, "please add Teams logo");
                    return View(teams);
                }
                if (imgFile != null)
                {//آپلود تصویر 
                    teams.Logo = Guid.NewGuid().ToString() + Path.GetExtension(imgFile.FileName);
                    string savePath = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/TeamLogo", teams.Logo
                    );

                    Upload uploader = new Upload();
                    uploader.UploadImage(savePath, imgFile);
                }
                _teamRepository.InsertTeam(teams);
                _teamRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(teams);
        }

        // GET: Admin/Teams/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["Leagues"] = new SelectList(_context.leagues, "Id", "Name");
            var teams = _teamRepository.GetTeamById(id.Value);
            if (teams == null)
            {
                return NotFound();
            }
            return View(teams);
        }

        // POST: Admin/Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("id,Name,Logo,LeagueId")] Teams teams, IFormFile imgFile)
        {
            if (id != teams.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ViewData["Leagues"] = new SelectList(_context.leagues, "Id", "Name");
                    var team = _teamRepository.GetTeamById(id);
                    if (imgFile != null)
                    {//آپلود تصویر 
                        teams.Logo = Guid.NewGuid().ToString() + Path.GetExtension(imgFile.FileName);
                        Delete Remover = new Delete();
                        Remover.DeleteImage(Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/TeamLogo", team.Logo));
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/TeamLogo", teams.Logo
                        );

                        Upload uploader = new Upload();
                        uploader.UploadImage(savePath, imgFile);
                        team.Logo = teams.Logo;
                    }
                    team.Name = teams.Name;
                    team.LeagueId = teams.LeagueId;
                    _teamRepository.UpdateTeam(team);
                    _teamRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeamsExists(teams.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(teams);
        }

        // GET: Admin/Teams/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teams = _teamRepository.GetTeamById(id.Value);
            if (teams == null)
            {
                return NotFound();
            }

            return View(teams);
        }

        // POST: Admin/Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                var teams = _teamRepository.GetTeamById(id);
                Delete Remover = new Delete();
                Remover.DeleteImage(Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot/TeamLogo", teams.Logo));
                _teamRepository.DeleteTeam(teams);
                _teamRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }
        }

        private bool TeamsExists(int id)
        {
            return _teamRepository.GetTeamById(id) != null;
        }
        public async Task<IActionResult> ShowGames(int id)
        {
            var games = await _context.schedules.Where(q => q.AwayTeamId == id || q.HomeTeamId == id).Include(i=>i.AwayTeams).Include(i => i.HomeTeams).ThenInclude(i => i.League).ToListAsync();
            return View(games);
        }
        public async Task<IActionResult> ShowResult(int id)
        {
            var results = await _context.Results.Where(q => q.Team1Id == id || q.Team2Id == id).Include(i => i.AwayTeams).Include(i => i.HomeTeams).ThenInclude(i => i.League).ToListAsync();
            return View(results);
        }
    }
}
