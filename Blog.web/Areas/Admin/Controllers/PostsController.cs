﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Blog.DataLayer.Context;
using Blog.DomainClasses.Post;
using Blog.Services.IRepositories;
using Blog.Utilities.Images;
using Blog.Utilities.String;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Blog.DomainClasses.PostCategory;

namespace Blog.web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class PostsController : Controller
    {
        private readonly IPostRepository _PostRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IPostCategoryRepository _PostCategoryRepository;
        private readonly IPostTagRepository _PostTagRepository;


        public PostsController(IPostRepository PostRepository, IUserRepository userRepository, ICategoryRepository categoryRepository, IPostCategoryRepository PostCategoryRepository, IPostTagRepository postTagRepository)
        {
            _PostRepository = PostRepository;
            _userRepository = userRepository;
            _categoryRepository = categoryRepository;
            _PostCategoryRepository = PostCategoryRepository;
            _PostTagRepository = postTagRepository;
        }

        // GET: Admin/Posts
        public IActionResult Index(bool isinterView = false)
        {
            if (isinterView == false)
            {
                ViewBag.isinterView = false;
                return View(_PostRepository.GetAllPosts());
            }
            else
            {
                ViewBag.isinterView = true;
                return View(_PostRepository.GetAllPosts().Where(q => q.IsInterView == true));
            }

        }

        // GET: Admin/Posts/Details/5
        public IActionResult Details(int? id)//جزئیات پست
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _PostRepository.GetPostById(id.Value);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Admin/Posts/Create
        public IActionResult Create(bool isinterView = false)
        {
            ViewBag.isinterView = isinterView;
            ViewData["Categories"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");
            return View();
        }

        // POST: Admin/Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("PostId,AuthorId,Title,ShortDesc,Description,ImageName,CreateDate,UpdateDate,IsPublish,IsInterView")] Post post, IFormFile imgFile, int cat = 0)
        {
            ViewData["Categories"] = new SelectList(_categoryRepository.GetAllCategories(), "CategoryId", "Name");
            if (ModelState.IsValid)
            {
                if (imgFile != null)
                {//آپلود تصویر 
                    post.ImageName = Guid.NewGuid().ToString() + Path.GetExtension(imgFile.FileName);
                    string savePath = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/PostImages", post.ImageName
                    );

                    Upload uploader = new Upload();
                    uploader.UploadImage(savePath, imgFile);
                }
                post.CreateDate = DateTime.Now;
                post.UpdateDate = DateTime.Now;
                post.IsPublish = false;
                post.VisitCount = 0;
                post.AuthorId = _userRepository.GetUserByName(User.Identity.Name).Id;//بدست آوردن شناسه نویسنده

                post.ShortKey = GetShortLink();//بدست آوردن کلید کوتاه

                _PostRepository.InsertPost(post);
                _PostRepository.Save();
                if (cat != 0)
                {
                    PostCategory postCategory = new PostCategory { CategoryId = cat, PostId = post.PostId };
                    _PostCategoryRepository.InsertPostCategory(postCategory);
                    _PostCategoryRepository.Save();
                }
                if (post.IsInterView == true)
                {
                    return RedirectToAction(nameof(Index), new { isinterView = true });

                }
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // GET: Admin/Posts/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _PostRepository.GetPostById(id.Value);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        // POST: Admin/Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("PostId,AuthorId,Title,ShortDesc,Description,ImageName,CreateDate,UpdateDate,IsPublish,IsInterView")] Post post, IFormFile imgFile)
        {
            if (id != post.PostId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (imgFile != null)//آپلود عکس
                    {
                        post.ImageName = post.ImageName == null ? Guid.NewGuid().ToString().Substring(0, 5) + ".jpg" : post.ImageName;
                        string savePath = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/PostImages", post.ImageName
                        );

                        Upload uploader = new Upload();
                        Task.Run(() => uploader.UploadImage(savePath, imgFile));
                    }

                    post.UpdateDate = DateTime.Now;
                    post.IsPublish = false;


                    _PostRepository.UpdatePost(post);//آپدیت پست
                    _PostRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(post.PostId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                if (post.IsInterView == true)
                {
                    return RedirectToAction(nameof(Index), new { isinterView = true });
                }
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // GET: Admin/Posts/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _PostRepository.GetPostById(id.Value);//دریافت اظلاعات پست 
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Admin/Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                var post = _PostRepository.GetPostById(id);
                bool isinterview = post.IsInterView;

                var postCategories = _PostCategoryRepository.GetPostCategoriesByPostId(post.PostId);
                foreach (var item in postCategories)
                {
                    _PostCategoryRepository.DeletePostCategory(item);
                    _PostCategoryRepository.Save();
                }
                var postTags = _PostTagRepository.GetPostTagByPostId(post.PostId);
                foreach (var item in postTags)
                {
                    _PostTagRepository.DeletePostTag(item);
                    _PostTagRepository.Save();
                }
                _PostRepository.DeletePost(post);
                if (post.ImageName != null)
                {
                    var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/PostImages", post.ImageName);
                    Utilities.Images.Delete delete = new Delete();
                    delete.DeleteImage(imagePath);
                }
                _PostRepository.Save();
                if (isinterview)
                {
                    return RedirectToAction(nameof(Index), new { isinterView = true });
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = "this opration not complete for relation cause";
                return RedirectToAction(nameof(Index));
            }
        }

        public IActionResult ChangeIsPublishStatus(int postId, bool isPublish, bool isinterView = false)//تغییر وضعیت انتشار پست
        {
            _PostRepository.PostPublishStatusChanger(postId, isPublish);
            if (isinterView)
            {
                return RedirectToAction(nameof(Index), new { isinterView = true });
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult ChangeSliderStatus(int Id, bool isPublish, bool isinterView = false)//تغییر وضعیت انتشار اسلایدر
        {
            _PostRepository.PostSliderStatusChanger(Id, isPublish);
            if (isinterView)
            {
                return RedirectToAction(nameof(Index), new { isinterView = true });
            }
            return RedirectToAction(nameof(Index));
        }
        private bool PostExists(int id)
        {
            return _PostRepository.PostExists(id);
        }

        private string GetShortLink()//بدست آوردن کلید کوتاه
        {
            string shortLink;
            int i = 0;
            int shortLinklength = 2;
            do
            {
                if (i > 2)
                {
                    shortLinklength++;
                    i = 0;
                }
                CreateString stringCreator = new CreateString();
                shortLink = stringCreator.CreateStringByLength(shortLinklength);
                i++;
            } while (!_PostRepository.ShortKeyIsValid(shortLink));

            return shortLink;
        }

    }
}
