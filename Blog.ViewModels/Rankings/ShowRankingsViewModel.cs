﻿using Blog.DomainClasses.Ranking;
using System.Collections.Generic;

namespace Blog.ViewModels.Rankings
{
    public class ShowRankingsViewModel
    {
        public List<Ranking> Rankings { get; set; }
        public int LeaugeId { get; set; }
    }
}
