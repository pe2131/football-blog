﻿using Blog.DomainClasses.league;
using Blog.DomainClasses.Option;
using Blog.DomainClasses.Ranking;
using Blog.DomainClasses.Results;
using Blog.DomainClasses.schedules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.web.Models.TablesViewModel
{
    public class TablesViewModel
    {
        public IEnumerable<Ranking> Rankings { get; set; }
        public IEnumerable<Results> Results { get; set; }
        public IEnumerable<schedules> Schedules { get; set; }
        public IEnumerable<League> leagues { get; set; }

        public Option SiteSetting { get; set; }
    }
}
