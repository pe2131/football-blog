﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.DomainClasses;
using Blog.DomainClasses.PostTag;

namespace Blog.ViewModels.Search
{
    public class PostsPaginationForSearchViewModel
    {
        public List<DomainClasses.Post.Post> Posts { get; set; }
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public string SearchWord { get; set; }
        public List<string> SimilarTags { get; set; }

    }
}
