﻿using Blog.DomainClasses.Results;
using System.Collections.Generic;

namespace Blog.ViewModels.Result
{
    public class ShowResultsViewModel
    {
        public List<Results> Results { get; set; }
        public int LeaugeId { get; set; }
    }
}
