﻿using Blog.DomainClasses.schedules;
using System.Collections.Generic;

namespace Blog.ViewModels.Schedules
{
    public class ShowSchedulesViewModel
    {
        public List<schedules> Schedules  { get; set; }
        public int LeaugeId { get; set; }
    }
}
