﻿using System.Collections.Generic;

namespace Blog.ViewModels.Post
{
    public class PostAndOptionViewModel
    {
        public List<DomainClasses.Post.Post> Posts { get; set; }

        public DomainClasses.Option.Option Option { get; set; }
    }
}
