﻿using System.Collections.Generic;

namespace Blog.ViewModels.Post
{
    public class PostInfiniteScrollViewModel
    {
        public List<DomainClasses.Post.Post> Posts { get; set; }
        public int TotalPages { get; set; }
        public int ItemPerPage { get; set; }
    }
}
