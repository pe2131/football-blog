﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.DomainClasses.Category;
using Blog.DomainClasses.Comment;
using Blog.DomainClasses.PostTag;

namespace Blog.ViewModels.Post
{
    public class ViewPostViewModel
    {
        public DomainClasses.Post.Post Post { get; set; }
        public List<PostTag> PostTags { get; set; }
        public List<Category> Categories { get; set; }

        public List<Comment> Comments { get; set; }
    }
}
