﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.ViewModels.Post
{
    public class PostsPaginationViewModel
    {
        public List<DomainClasses.Post.Post> Posts { get; set; }
        public DomainClasses.Option.Option option { get; set; }
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
    }
}
