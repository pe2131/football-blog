﻿using Blog.DomainClasses.commentForSlider;
using Blog.DomainClasses.Slider;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.ViewModels.Gallery
{
    public class GalleryViewModel
    {
        public List<Slider> Other { get; set; }
        public Slider Current { get; set; }
        public List<CommentForSlider> Comments { get; set; }


    }
}
